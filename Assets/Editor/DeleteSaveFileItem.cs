﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class DeleteSaveFileItem
    {
        [MenuItem("Window/Custom/Delete Save File")]
        private static void DeleteSaveFile()
        {
            File.Delete(Path.Combine(Application.persistentDataPath,"SaveGame.json"));
        }
    }
}