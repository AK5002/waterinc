﻿using System;
using SFX;
using UnityEngine;

namespace Core.Game
{
    public interface ISettings
    {
        bool IsSoundOn { get; }
        bool IsMusicOn { get; }
        bool IsEffectsOn { get; }
    }
}