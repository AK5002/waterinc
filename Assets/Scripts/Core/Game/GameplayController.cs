﻿using System.Collections.Generic;
using Core.Game.Bank;
using Core.Game.ProductionLine;
using Core.Game.Statistics;
using Core.Mechanics.SectorSystem;
using Managers;
using Services.DependencyInjection;
using Services.InGameNotificationSystem;
using Services.SaveService;
using SFX;
using UI;
using UI.Views;
using UI.Views.FactoryMenu;
using UnityEngine;


namespace Core.Game
{
    public class GameplayController : MonoBehaviour, IGameplaySettings
    {
        [SerializeField] private ProblemManager m_ProblemManager;
        [SerializeField] private ConveyorController m_ConveyorController;
        [SerializeField] private SectorController m_SectorController;
        [SerializeField] private NotificationController m_NotificationController;

        [SerializeField] private GameplaySettings m_GameplaySettings;

        private SaveManager saveManager;
        private UIManager uiManager;
        private UISoundsController uiSoundsController;

        public static IGameplaySettings GameplaySettings;
        public bool IsGamePaused { get; private set; }
        public bool IsInSectorMapView { get; private set; }
        public float LiterToDollarRatio => m_GameplaySettings.LiterToDollarRatio;
        public IEnumerable<double> ConveyorPrices => m_GameplaySettings.ConveyorPrices;

        public void Initialize(SaveManager saveManager, UIManager uiManager, UISoundsController uiSoundsController)
        {
            this.saveManager = saveManager;
            this.uiManager = uiManager;
            this.uiSoundsController = uiSoundsController;

            GameplaySettings = this;

            // if (!saveManager.CurrentSaveObject.IsFTUEComplete)
            // {
            //     Debug.Log(Injector.Instance);
            //     var ftueController = Injector.Instance.GetInstanceOf<FTUEController>();
            //     ftueController.gameObject.SetActive(true);
            //     ftueController.SwitchToNextHolder();
            // }
        }

        public void Setup()
        {
            m_ConveyorController.Setup(uiManager, m_NotificationController, saveManager.CurrentSaveObject.SaveDatas,
                uiSoundsController);
            m_SectorController.Setup(uiManager, saveManager.CurrentSaveObject.SectorDatas, uiSoundsController);
            m_ProblemManager.Setup(saveManager.CurrentSaveObject.CustomerServiceRating, m_SectorController,
                m_NotificationController, uiSoundsController);
            PlayerBank.InitializeInstance(this, saveManager.CurrentSaveObject.Money,
                m_ConveyorController.ActiveConveyors,
                (m_SectorController as ISectorControllerStatistics).PlayerSectors);
            PlayerStatistics.InitializeInstance(m_ConveyorController.ActiveConveyors, PlayerBank.Instance,
                m_ProblemManager);

            IsGamePaused = false;
        }

        public void OnFactoryViewEnter()
        {
            uiManager.ViewController.Show<FactoryView,UIData> (null,0.3f,0.3f);
            m_ConveyorController.gameObject.SetActive(true);
        }

        public void OnFactoryViewExit()
        {
            m_ConveyorController.gameObject.SetActive(false);
        }

        private void Update()
        {
            IsGamePaused = uiManager.PopUpController.CurrentPopUp != null;
            IsInSectorMapView = uiManager.ViewController.CurrentView?.Group == typeof(SectorMapView);
        }

        public void Deconstruct(out ProblemManager problemManager, out ConveyorController conveyorController, out SectorController sectorController)
        {
            problemManager = m_ProblemManager;
            conveyorController = m_ConveyorController;
            sectorController = m_SectorController;
        }
    }
}