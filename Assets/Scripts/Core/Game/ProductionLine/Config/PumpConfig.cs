﻿using System;
using UnityEngine;

namespace Core.Game.ProductionLine.Config
{
    [CreateAssetMenu(menuName = "ScriptableObjects/PumpConfig", fileName = "pumpConfig", order = 0)]
    public class PumpConfig : ComponentConfig
    {
        public override Type Type => typeof(PumpConfig);
    }
}
