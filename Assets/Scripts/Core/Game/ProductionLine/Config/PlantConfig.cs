﻿using System;
using UnityEngine;

namespace Core.Game.ProductionLine.Config
{
    [CreateAssetMenu(menuName ="ScriptableObjects/TreatmentPlantConfig", fileName = "PlantConfig", order = 0)]
    public class PlantConfig : ComponentConfig
    {
        public override Type Type => typeof(PlantConfig);
    }
}
