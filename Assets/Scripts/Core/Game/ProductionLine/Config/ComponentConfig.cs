﻿using System;
using UnityEngine;

namespace Core.Game.ProductionLine.Config
{
    public abstract class ComponentConfig : ScriptableObject
    {
        [SerializeField] private Sprite m_Icon;
        [SerializeField] private string m_ComponentName;
        [SerializeField] private int m_ID;
        [SerializeField] private double m_Price;
        [SerializeField] private double m_ProductionPerSec;
        [SerializeField] private double m_CostPerSec;

        public abstract Type Type { get; }
        public Sprite Icon => m_Icon;
        public string ComponentName => m_ComponentName;
        public int ID => m_ID;
        public double Price => m_Price;
        public double ProductionPerSec => m_ProductionPerSec;
        public double CostPerSec => m_CostPerSec;
    }
}