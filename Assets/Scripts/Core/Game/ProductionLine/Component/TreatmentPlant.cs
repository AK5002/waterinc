﻿using System;
using Core.Game.ProductionLine.Config;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Utilities.UI;

namespace Core.Game.ProductionLine.Component
{
    public class TreatmentPlant : FactoryComponent
    {
        [SerializeField] private PlantConfig m_CurrentConfig;

        public PlantConfig CurrentConfig
        {
            get => m_CurrentConfig;
            set
            {
                var sequence = DOTween.Sequence();
                sequence.Append(m_CanvasGroup.DOFade(0, m_CurrentConfig == null ? 0 : 1).SetEase(Ease.InSine)).OnComplete(() =>
                {
                    m_ComponentImage.sprite = value.Icon;
                    m_ProductionText.text =  $"{UIUtils.NumberToDecimalNotation(value.ProductionPerSec.ToString(),0)} L/SEC";
                    m_CanvasGroup.DOFade(1, 1).SetEase(Ease.InSine);
                });
                
                m_CurrentConfig = value;
            }
        }

        public override void Setup(Action<ComponentConfig> onStoreButtonPresed, Action onPlayButtonClickSound)
        {
            base.Setup(onStoreButtonPresed,onPlayButtonClickSound);
            
            m_StoreButton.onClick.RemoveAllListeners();
            m_StoreButton.onClick.AddListener(() =>
            {
                onPlayButtonClickSound();
                onStoreButtonPresed(CurrentConfig);
            });
        }
    }
}
