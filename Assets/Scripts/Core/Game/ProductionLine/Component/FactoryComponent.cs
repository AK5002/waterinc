﻿using System;
using System.Collections.Generic;
using Core.Game.ProductionLine.Config;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Game.ProductionLine.Component
{
    public abstract class FactoryComponent : MonoBehaviour, IMenuObservable, IMenuObserver
    {
        [SerializeField] protected Image m_ComponentImage;
        [SerializeField] protected CanvasGroup m_CanvasGroup;
        [SerializeField] protected Button m_StoreButton;
        [SerializeField] protected Button m_ComponentButton;
        [SerializeField] protected TMP_Text m_ProductionText;

        [SerializeField] private float m_MenuButtonStartPos;
        [SerializeField] private float m_MenuButtonEndPos;

        private bool isComponentMenuOpen;
        private bool isMoving;

        public List<IMenuObserver> Observers { get; private set; }

        public virtual void Setup(Action<ComponentConfig> onStoreButtonPresed, Action onPlayButtonClickSound)
        {
            Observers = new List<IMenuObserver>();

            m_StoreButton.gameObject.SetActive(false);
            m_ProductionText.gameObject.SetActive(false);

            m_ComponentButton.onClick.RemoveAllListeners();
            m_ComponentButton.onClick.AddListener(() =>
            {
                onPlayButtonClickSound();
                if (isComponentMenuOpen && !isMoving)
                {
                    CloseComponentMenu();
                }
                else if (!isMoving)
                {
                    OpenComponentMenu();
                }
            });
        }

        private void CloseComponentMenu()
        {
            if (!isComponentMenuOpen) return;

            isMoving = true;
            var storeButtonRectTransform = (RectTransform) m_StoreButton.transform;
            var productionTextRectTransform = (RectTransform) m_ProductionText.transform;
            storeButtonRectTransform
                .DOAnchorPos(new Vector2(storeButtonRectTransform.anchoredPosition.x, m_MenuButtonStartPos), 1f)
                .SetEase(Ease.InSine);
            productionTextRectTransform
                .DOAnchorPos(new Vector2(productionTextRectTransform.anchoredPosition.x, m_MenuButtonStartPos), 1f)
                .SetEase(Ease.InSine)
                .OnComplete(() =>
                {
                    m_StoreButton.gameObject.SetActive(false);
                    m_ProductionText.gameObject.SetActive(false);

                    isComponentMenuOpen = false;
                    isMoving = false;
                });

            Debug.Log("Opening");
        }

        private void OpenComponentMenu()
        {
            if (isComponentMenuOpen) return;

            Notify();

            isMoving = true;
            m_StoreButton.gameObject.SetActive(true);
            m_ProductionText.gameObject.SetActive(true);

            var storeButtonRectTransform = (RectTransform) m_StoreButton.transform;
            var productionTextRectTransform = (RectTransform) m_ProductionText.transform;
            storeButtonRectTransform
                .DOAnchorPos(new Vector2(storeButtonRectTransform.anchoredPosition.x, m_MenuButtonEndPos), 1f)
                .SetEase(Ease.InSine);
            productionTextRectTransform
                .DOAnchorPos(new Vector2(productionTextRectTransform.anchoredPosition.x, m_MenuButtonEndPos), 1f)
                .SetEase(Ease.InSine).OnComplete(() => isMoving = false);

            Debug.Log("Closing");

            isComponentMenuOpen = true;
        }

        public void Attach(params IMenuObserver[] observers)
        {
            foreach (var observer in observers)
            {
                Observers.Add(observer);
            }
        }

        public void Detach(IMenuObserver observer)
        {
            if (Observers.Contains(observer))
                Observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in Observers)
            {
                observer.UpdateObserver(this);
            }
        }

        public void UpdateObserver(IMenuObservable subject)
        {
            if ((FactoryComponent) subject != this)
                CloseComponentMenu();
        }
    }
}