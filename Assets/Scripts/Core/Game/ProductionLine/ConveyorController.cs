﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game.ProductionLine.Config;
using Managers;
using Services.InGameNotificationSystem;
using SFX;
using UI.PopUps.ComponentPurchasePopUp;
using UI.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Game.ProductionLine
{
    public class ConveyorController : MonoBehaviour
    {
        [SerializeField] private List<ConveyorHolder> m_ConveyorHolders;
        [SerializeField] private List<PumpConfig> m_PumpConfigs;
        [SerializeField] private List<PlantConfig> m_PlantConfigs;

        private IEnumerable<Conveyor> Conveyors;
        public List<Conveyor> ActiveConveyors { get; private set; }

        public List<PumpConfig> PumpConfigs => m_PumpConfigs;
        public List<PlantConfig> PlantConfigs => m_PlantConfigs;
        
        private UIManager uiManager;
        private NotificationController notificationController;
        
        private Action onPlayButtonClickSound;

        public void Setup(UIManager uiManager,
            NotificationController notificationController,List<ConveyorSaveData> saveDatas, UISoundsController uiSoundsController)
        {
            this.uiManager = uiManager;
            this.notificationController = notificationController;
            onPlayButtonClickSound = uiSoundsController.OnPlayButtonClickSound;
            
            ActiveConveyors = new List<Conveyor>();
            Conveyors = m_ConveyorHolders.Select(conveyorHolder => conveyorHolder.Conveyor);
            
            foreach (var conveyorHolder in m_ConveyorHolders)
            {
                conveyorHolder.OnConveyorAdd += AddConveyor;
                conveyorHolder.Setup(saveDatas.FirstOrDefault(data => data.ID == conveyorHolder.ConveyorID),
                    notificationController.AddNotification,onPlayButtonClickSound);
            }
        }

        private void AddConveyor(ConveyorSaveData data)
        {
            var conveyor = Conveyors.FirstOrDefault(conveyorToFind => conveyorToFind.ID == data.ID);

            if (conveyor == null)
            {
                Debug.Log("Could not find a convyeor");
                return;
            }

            conveyor.OnGetPlant += (id) => m_PlantConfigs.FirstOrDefault(config => config.ID == id);
            conveyor.OnNotificate += (message) =>
                notificationController.AddNotification(message, NotificationNature.ErrorNotification);
            conveyor.OnComponentButtonPressed += OnComponentMenuPressed;
            var plant = m_PlantConfigs.FirstOrDefault(plantConfig => plantConfig.ID == data.PlantID);
            conveyor.Setup(m_PumpConfigs.FirstOrDefault(pumpConfig => pumpConfig.ID == data.PumpID),
                plant, data.AvailablePumpIDs,data.AvailablePlantIDs,onPlayButtonClickSound);

            ActiveConveyors.Add(conveyor);

            foreach (var activeConveyor in ActiveConveyors)
            {
                activeConveyor.PlantObservable.Attach(conveyor.PlantObserver, conveyor.PumpObserver);
                activeConveyor.PumpObservable.Attach(conveyor.PumpObserver,conveyor.PlantObserver);

                conveyor.PlantObservable.Attach(activeConveyor.PlantObserver,activeConveyor.PumpObserver);
                conveyor.PumpObservable.Attach(activeConveyor.PumpObserver,activeConveyor.PlantObserver);
            }
        }

        public bool SetConfigOfComponent(int conveyorID, ComponentConfig config, bool buy)
        {
            var conveyor = ActiveConveyors.FirstOrDefault(conveyor1 => conveyor1.ID == conveyorID);
            if (conveyor == null)
            {
                Debug.Log("Invalid Conveyor ID");
                return false;
            }

            if (config.Type == typeof(PumpConfig))
            {
                return conveyor.LoadNewPump((PumpConfig) config, buy);
            }
            else
            {
                return conveyor.LoadNewPlant((PlantConfig) config, buy);
            }
        }

        private void OnComponentMenuPressed(Type type, PurchaseMenuData data, Func<Type,PurchaseMenuData> onUpdate)
        {
            var configs = type == typeof(PumpConfig)
                ? m_PumpConfigs.Select(config => config as ComponentConfig)
                : m_PlantConfigs.Select(config => config as ComponentConfig);
            
            data.Configs = configs.ToList();
            data.OnButtonPressed = (config, buy) => SetConfigOfComponent(data.ConveyorID, config, buy);
            data.OnDataUpdate = () => onUpdate(type);

            uiManager.PopUpController.Show<PurchasePopUp, PurchaseMenuData>(data);
        }

        public List<ConveyorSaveData> CreateSaveDataList()
        {
            return ActiveConveyors.Select(conveyor => conveyor.CreateSaveData()).ToList();
        }
    }
}