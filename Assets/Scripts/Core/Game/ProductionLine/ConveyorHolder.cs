﻿using System;
using System.Linq;
using Core.Game.Bank;
using DG.Tweening;
using Services.InGameNotificationSystem;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utilities;
using Utilities.UI;

namespace Core.Game.ProductionLine
{
    public class ConveyorHolder : MonoBehaviour
    {
        [SerializeField] private Conveyor m_Conveyor;
        [FormerlySerializedAs("m_PurchasePlantButton")] [SerializeField] private Button m_PurchaseConveyorButton;
        [SerializeField] private TMP_Text m_PriceText;
        [SerializeField] private CanvasGroup m_PriceHolderGroup;

        public Conveyor Conveyor => m_Conveyor;
        public int ConveyorID => m_Conveyor.ID;
        
        public event Action<ConveyorSaveData> OnConveyorAdd;
        
        public void Setup(ConveyorSaveData saveData, Action<string,NotificationNature> notificate, Action onPlayButtonClickSound)
        {
            m_PriceText.text = $"{UIUtils.NumberToDecimalNotation(GameplayController.GameplaySettings.ConveyorPrices.ToList()[ConveyorID].ToString(),0)} W";
            if (saveData == null)
            {
                m_Conveyor.gameObject.SetActive(false);
                m_PurchaseConveyorButton.gameObject.SetActive(true);

                m_PurchaseConveyorButton.onClick.AddListener(() =>
                {
                    onPlayButtonClickSound();
                    if (!PlayerBank.Instance.TakeAmount(GameplayController.GameplaySettings.ConveyorPrices.ToList()[ConveyorID]))
                    {
                        notificate?.Invoke("Not enough Money!", NotificationNature.ErrorNotification);
                        return;
                    }

                    m_PurchaseConveyorButton.gameObject.SetActive(false);
                    m_PriceHolderGroup.DOFade(0, 1f).SetEase(Ease.InSine)
                        .OnComplete(() => m_PriceHolderGroup.gameObject.SetActive(false));
                    OnConveyorAdd?.Invoke(new ConveyorSaveData(){ID = Conveyor.ID,PumpID = 0,PlantID = 0});
                });
            }
            else
            {
                m_PurchaseConveyorButton.gameObject.SetActive(false);
                m_PriceHolderGroup.gameObject.SetActive(false);
                
                OnConveyorAdd?.Invoke(saveData);
            }
        }
    }
}