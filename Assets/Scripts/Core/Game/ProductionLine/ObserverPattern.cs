﻿using System.Collections.Generic;

namespace Core.Game.ProductionLine
{
    public interface IMenuObserver
    {
        void UpdateObserver(IMenuObservable subject);
    }

    public interface IMenuObservable
    {
        List<IMenuObserver> Observers { get; }

        void Attach(params IMenuObserver[] observers);
        void Detach(IMenuObserver observer);

        void Notify();
    }
}