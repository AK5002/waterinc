﻿using System;
using System.Collections.Generic;
using Core.Game.Bank;
using Core.Game.ProductionLine.Component;
using Core.Game.ProductionLine.Config;
using TMPro;
using UI.PopUps.ComponentPurchasePopUp;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Game.ProductionLine
{
    public class Conveyor : MonoBehaviour, IConveyorStatistics
    {
        [SerializeField] private Pump m_Pump;
        [SerializeField] private TreatmentPlant m_Plant;
        [SerializeField] private Button m_PurchasePlantButton;
        [SerializeField] private TMP_Text m_CleanWaterPercentText;
        [SerializeField] private int m_ID;

        public HashSet<int> AvailalePumpIDs { get; private set; }
        public HashSet<int> AvailalePlantIDs { get; private set; }
        
        public int ID => m_ID;
        public double ProductionPerSec => m_Pump.CurrentConfig.ProductionPerSec;
        public double CostPerSec => m_Pump.CurrentConfig.CostPerSec + m_Plant.CurrentConfig?.CostPerSec ?? 0;

        public double CleanWaterToAllRatio => Mathf.Clamp((float)((m_Plant.CurrentConfig?.ProductionPerSec ?? 0) / m_Pump.CurrentConfig.ProductionPerSec),0f,1f);

        public event Func<int, PlantConfig> OnGetPlant;
        public event Action<string> OnNotificate;
        public event Action<Type,PurchaseMenuData,Func<Type,PurchaseMenuData>> OnComponentButtonPressed;

        public IMenuObservable PumpObservable => m_Pump;
        public IMenuObservable PlantObservable => m_Plant;
        
        public IMenuObserver PumpObserver => m_Pump;
        public IMenuObserver PlantObserver => m_Plant;

        public void Setup(PumpConfig pumpConfig, PlantConfig plantConfig, List<int> availablePumpIDs, List<int> availablePlantIDs, Action onPlayButtonClickSound)
        {
            this.AvailalePumpIDs = availablePumpIDs != null ? new HashSet<int>(availablePumpIDs) : new HashSet<int>();
            this.AvailalePlantIDs = availablePlantIDs != null ? new HashSet<int>(availablePlantIDs) : new HashSet<int>();
            
            gameObject.SetActive(true);
            m_Pump.Setup((config) => OnComponentButtonPressed?.Invoke(typeof(PumpConfig),CreateMenuData(typeof(PumpConfig)),CreateMenuData),onPlayButtonClickSound);
            m_Plant.Setup((config) => OnComponentButtonPressed?.Invoke(typeof(PlantConfig),CreateMenuData(typeof(PlantConfig)),CreateMenuData),onPlayButtonClickSound);
            LoadNewComponents(pumpConfig,plantConfig);
            m_PurchasePlantButton.onClick.AddListener(() => LoadNewPlant(OnGetPlant?.Invoke(1),true));
            
        }

        public void LoadNewComponents(PumpConfig pumpConfig, PlantConfig plantConfig)
        {
            LoadNewPump(pumpConfig,false);
            LoadNewPlant(plantConfig,false);
        }

        public bool LoadNewPump(PumpConfig pumpConfig,bool buy)
        {
            if (pumpConfig == null) return false;

            if (buy && !PlayerBank.Instance.TakeAmount(pumpConfig.Price))
            {
                OnNotificate?.Invoke("Not enough money!");
                return false;
            }
            
            m_Pump.CurrentConfig = pumpConfig;
            m_CleanWaterPercentText.text= $"{(int)(CleanWaterToAllRatio * 100)}%";

            AvailalePumpIDs.Add(pumpConfig.ID);

            return true;
        }

        public bool LoadNewPlant(PlantConfig plantConfig, bool buy)
        {
            if (plantConfig == null) return false;

            if (buy && !PlayerBank.Instance.TakeAmount(plantConfig.Price))
            {
                OnNotificate?.Invoke("Not enough money!");
                return false;
            }

            Debug.Log("Loading new Plant...");
            m_Plant.gameObject.SetActive(true);
            m_PurchasePlantButton.gameObject.SetActive(false);
            m_Plant.CurrentConfig = plantConfig;
            m_CleanWaterPercentText.text = $"{(int)(CleanWaterToAllRatio * 100)}%";
            AvailalePlantIDs.Add(plantConfig.ID);

            return true;
        }
        
        public ConveyorSaveData CreateSaveData()
        {
            var plantID = m_Plant.CurrentConfig?.ID ?? 0;
            var saveData = new ConveyorSaveData()
            {
                ID = m_ID, 
                PumpID = m_Pump.CurrentConfig.ID, 
                PlantID = plantID,
                AvailablePumpIDs = new List<int>(AvailalePumpIDs),
                AvailablePlantIDs = new List<int>(AvailalePlantIDs)
            };
            return saveData;
        }

        public PurchaseMenuData CreateMenuData(Type type)
        {
            var data = new PurchaseMenuData()
            {
                CurrentConfig = type == typeof(PumpConfig) ? (ComponentConfig)m_Pump.CurrentConfig : m_Plant.CurrentConfig,
                AvailableConfigIDs = type == typeof(PumpConfig) ? AvailalePumpIDs : AvailalePlantIDs,
                ConveyorID = m_ID
            };

            return data;
        }
    }

    public class ConveyorSaveData
    {
        public int ID;
        public int PumpID;
        public int PlantID;

        public List<int> AvailablePumpIDs;
        public List<int> AvailablePlantIDs;
    }
    
    public interface IConveyorStatistics
    {
        double ProductionPerSec { get; }
        double CleanWaterToAllRatio { get; }
        double CostPerSec { get; }
    }
}
