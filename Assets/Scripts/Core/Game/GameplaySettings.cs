﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Game
{
    [CreateAssetMenu(fileName = "gameplaySettings", menuName = "ScriptableObjects/GameplaySettings", order = 0)]
    public class GameplaySettings : ScriptableObject
    {
        [SerializeField] private List<double> m_ConveyorPrices;
        [SerializeField] [Range(0,1)] private float m_LiterToDollarRatio;

        public IEnumerable<double> ConveyorPrices => m_ConveyorPrices.OrderBy((price) => price);
        public float LiterToDollarRatio => m_LiterToDollarRatio;
    }
    
    public interface IGameplaySettings
    {
        bool IsGamePaused { get; }
        bool IsInSectorMapView { get; }
        float LiterToDollarRatio { get; }
        IEnumerable<double> ConveyorPrices { get; }
    }
}