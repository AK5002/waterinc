﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game.ProductionLine;
using Core.Game.Statistics;
using Core.Mechanics.SectorSystem;
using UnityEngine;

namespace Core.Game.Bank
{
    public class PlayerBank : AbstractBank,IBankStatistics
    {
        public static PlayerBank Instance { get; private set; }

        public sealed override double Money { get; protected set; }
        public override double ProfitPerSec => Math.Floor(incomes.Sum() - expenses.Sum());

        private readonly IEnumerable<IConveyorStatistics> conveyorStatistics;
        private readonly IEnumerable<SectorConfig> sectorConfigs;

        private IEnumerable<double> incomes => sectorConfigs.Select(config => config.ProfitPerSec * ((PlayerStatistics.Instance?.Rating ?? 5f) / 5)); 

        private IEnumerable<double> expenses => conveyorStatistics.Select(conveyor => conveyor.CostPerSec);

        private PlayerBank(MonoBehaviour monoBehaviour,double money, IEnumerable<IConveyorStatistics> conveyorStatistics, IEnumerable<SectorConfig> sectorConfigs) : base(monoBehaviour, money)
        {
            Money = money;
            this.conveyorStatistics = conveyorStatistics;
            this.sectorConfigs = sectorConfigs;
            monoBehaviour.StartCoroutine(DepositProfit());
        }
        
        public static void InitializeInstance(MonoBehaviour monoBehaviour,double money, IEnumerable<IConveyorStatistics> conveyorStatistics, IEnumerable<SectorConfig> sectorConfigs)
        {
            if(Instance == null)
                Instance = new PlayerBank(monoBehaviour,money,conveyorStatistics,sectorConfigs);
        }

        protected sealed override IEnumerator DepositProfit()
        {
            return base.DepositProfit();
        }
    }
    
    public interface IBankStatistics
    {
        double Money { get; }
        double ProfitPerSec { get; }
    }
}
