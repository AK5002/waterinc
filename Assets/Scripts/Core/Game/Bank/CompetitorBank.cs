﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game.ProductionLine.Config;
using UnityEngine;


namespace Core.Game.Bank
{
    public class CompetitorBank : AbstractBank
    {
        public sealed override double Money { get; protected set; }
        public override double ProfitPerSec => incomes.Sum() - expenses.Sum();
        
        private IEnumerable<double> incomes => conveyors.Select(conveyor => conveyor.Item1.ProductionPerSec * GameplayController.GameplaySettings.LiterToDollarRatio);

        private IEnumerable<double> expenses =>
            conveyors.Select(conveyor => conveyor.Item1.CostPerSec + conveyor.Item2.CostPerSec);

        private readonly IEnumerable<Tuple<PumpConfig, PlantConfig>> conveyors;
            
        public CompetitorBank(MonoBehaviour monoBehaviour, double money, IEnumerable<Tuple<PumpConfig,PlantConfig>> conveyors) : base(monoBehaviour, money)
        {
            Money = money;
            monoBehaviour.StartCoroutine(DepositProfit());
            this.conveyors = conveyors;
        }

        protected sealed override IEnumerator DepositProfit()
        {
            return base.DepositProfit();
        }
    }
}