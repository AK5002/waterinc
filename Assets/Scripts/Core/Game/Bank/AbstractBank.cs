﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.Game.Bank
{
    public abstract class AbstractBank : IBankStatistics
    {
        public abstract double Money { get; protected set; }
        public abstract double ProfitPerSec { get; }

        protected AbstractBank(MonoBehaviour monoBehaviour, double money)
        {
        }

        public virtual bool TakeAmount(double amount)
        {
            if (Money < amount) return false;
            Money -= amount;
            return true;
        }

        public virtual void AddAmount(double amount)
        {
            Money += amount;
        }

        protected virtual IEnumerator DepositProfit()
        {
            while (true)
            {
                if (!GameplayController.GameplaySettings.IsGamePaused && !GameplayController.GameplaySettings.IsInSectorMapView)
                {
                    Money += Math.Floor(ProfitPerSec);    
                }
                
                yield return new WaitForSeconds(1);
            }
        }
    }
}