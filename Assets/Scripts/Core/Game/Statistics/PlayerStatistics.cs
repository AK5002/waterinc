﻿using System.Collections.Generic;
using System.Linq;
using Core.Game.Bank;
using Core.Game.ProductionLine;
using Managers;
using UnityEngine;

namespace Core.Game.Statistics
{
    public class PlayerStatistics : IStatisticsData
    {
        public static IStatisticsData Instance { get; private set; }
        
        private readonly IEnumerable<IConveyorStatistics> conveyorStatistics;
        private readonly IBankStatistics bankStatistics;
        private readonly IProblemServiceStatistics problemServiceStatistics;
        
        public double Money => bankStatistics.Money;
        public double ProfitPerSec => bankStatistics.ProfitPerSec;
        public double ProductionPerSec => conveyorStatistics.Sum(conveyor => conveyor.ProductionPerSec);
        public double CleanWaterPercentage => conveyorStatistics.Sum(conveyor => conveyor.CleanWaterToAllRatio) * 100;

        private double waterQualityRating =>
            (conveyorStatistics.Sum(conveyor => conveyor.CleanWaterToAllRatio) / conveyorStatistics.ToList().Count) * 5;

        private double customerServiceRating => problemServiceStatistics.PlayerCustomerServiceRating;
        public double Rating => Mathf.Clamp((float)(waterQualityRating + customerServiceRating), 2, 10);
        public double CustomerServiceRating => customerServiceRating;
        public double WaterQualityRating => waterQualityRating;

        private PlayerStatistics(IEnumerable<IConveyorStatistics> conveyorStatistics, IBankStatistics bankStatistics,IProblemServiceStatistics problemServiceStatistics)
        {
            this.conveyorStatistics = conveyorStatistics;
            this.bankStatistics = bankStatistics;
            this.problemServiceStatistics = problemServiceStatistics;
        }

        public static void InitializeInstance(IEnumerable<IConveyorStatistics> conveyorStatistics,
            IBankStatistics bankStatistics,IProblemServiceStatistics problemServiceStatistics)
        {
            if(Instance == null)
                Instance = new PlayerStatistics(conveyorStatistics,bankStatistics,problemServiceStatistics);
        }
    }

    public interface IStatisticsData
    {
        double Rating { get; }
        double CustomerServiceRating { get; }
        double WaterQualityRating { get; }
        double Money { get; }
        double ProfitPerSec { get; }
        double ProductionPerSec { get; }
        double CleanWaterPercentage{get;}
    }
}
