﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Bank;
using Core.Game.ProductionLine;
using Core.Game.ProductionLine.Config;
using UnityEngine;

namespace Core.Game.Statistics
{
    public class CompetitorStatistics : IStatisticsData
    {
        public double Rating => Mathf.Clamp((float) (cleanWaterRating + _statisticsData.CustomerServiceRating), 1f, 5f);
        public double CustomerServiceRating { get; }
        public double WaterQualityRating { get; }
        public double CleanWaterPercentage => cleanWaterRating * 20;
        private double cleanWaterRating =>
            (_statisticsData.Conveyors.Select(conveyor => conveyor.Item2.ProductionPerSec).Sum() / ProfitPerSec) * 5;

        public double Money => bankStatistics.Money;
        public double ProfitPerSec => bankStatistics.ProfitPerSec;

        public double ProductionPerSec =>
            _statisticsData.Conveyors.Select(conveyor => conveyor.Item1.ProductionPerSec).Sum();

        private readonly ICompetitorStatisticsData _statisticsData;
        private readonly IBankStatistics bankStatistics;

        public CompetitorStatistics(ICompetitorStatisticsData statisticsData)
        {
            this._statisticsData = statisticsData;
            this.bankStatistics = statisticsData.BankStatistics;
        }
    }

    public interface ICompetitorStatisticsData
    {
        IEnumerable<Tuple<PumpConfig, PlantConfig>> Conveyors { get; }
        IBankStatistics BankStatistics { get; }
        double CustomerServiceRating { get; }
        
    }
}