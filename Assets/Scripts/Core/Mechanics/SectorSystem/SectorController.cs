﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Statistics;
using Core.Mechanics.ProblemSystem;
using Managers;
using SFX;
using UnityEngine;

namespace Core.Mechanics.SectorSystem
{
    public class SectorController : MonoBehaviour, IProblemSystemSectorController, ISectorControllerStatistics
    {
        [SerializeField] private List<Sector> m_Sectors;

        IEnumerable<SectorConfig> ISectorControllerStatistics.PlayerSectors =>
            m_Sectors.Where(sector => sector.OwnerID == 1).Select(sector => sector.SectorConfig);

        IEnumerable<int> IProblemSystemSectorController.FreeSectorIDs => m_Sectors
            .Where(sector => sector.CurrentProblem == null && sector.OwnerID == 1).Select(sector => sector.SectorConfig.ID);

        IEnumerable<int> IProblemSystemSectorController.PlayerSectorIDs =>
            m_Sectors.Where(sector => sector.OwnerID == 1).Select(sector => sector.SectorConfig.ID);
        
        public void Setup(UIManager uiManager, List<SectorData> sectorDatas,UISoundsController uiSoundsController)
        {
            foreach (var sector in m_Sectors)
            {
                sector.Setup(sectorDatas?.FirstOrDefault(data => data.ID == sector.SectorConfig.ID), uiManager, uiSoundsController.OnPlayButtonClickSound);
            }
        }

        void IProblemSystemSectorController.AddProblemToSector(Problem problem, int sectorID)
        {
            var sector = m_Sectors.FirstOrDefault(sectorToFind => sectorToFind.SectorConfig.ID == sectorID);
            if (sector == null)
            {
                Debug.Log("Could not Find Sector");
                return;
            }

            sector.CurrentProblem = problem;
        }

        public void RemoveProblem(Problem problem)
        {
            var sector = m_Sectors.FirstOrDefault(sectorToFind => sectorToFind.SectorConfig.ID == problem.SectorID);
            if (sector == null || sector.CurrentProblem != problem) return;

            sector.CurrentProblem = null;
        }

        public void SetOwnerOfSector(int id)
        {
            var sector = m_Sectors.FirstOrDefault(sector1 => sector1.SectorConfig.ID == 1);
            if (sector == null) return;

            sector.OwnerID = id;
        }

        public List<SectorData> CreateSectorDataList()
        {
            return new List<SectorData>(m_Sectors.Select(sector => sector.CreateSectorData()));
        }
    }

    public interface IProblemSystemSectorController
    {
        IEnumerable<int> FreeSectorIDs { get; }
        IEnumerable<int> PlayerSectorIDs { get; }
        void AddProblemToSector(Problem problem, int sectorID);
        void RemoveProblem(Problem problem);
    }

    public interface ISectorControllerStatistics
    {
        IEnumerable<SectorConfig> PlayerSectors { get; }
    }
}