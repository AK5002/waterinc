﻿using System;
using Core.Game.ProductionLine.Config;
using Core.Game.Statistics;
using Core.Mechanics.ProblemSystem;
using DG.Tweening;
using Managers;
using TMPro;
using UI.PopUps.SectorInfoPopUp;
using UI.Views.SectorViewMenu;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Utilities.UI;

namespace Core.Mechanics.SectorSystem
{
    public class Sector : MonoBehaviour
    {
        [SerializeField] private SectorConfig m_SectorConfig;

        [SerializeField] private Animator m_WarningAnimator;

        [SerializeField] private Button m_InfoButton;
        [SerializeField] private Button m_SectorButton;
        [SerializeField] private Button m_AcquireButton;

        [SerializeField] private TMP_Text m_SectorName;
        [SerializeField] private CanvasGroup m_LockImageCanvasGroup;

        public SectorConfig SectorConfig => m_SectorConfig;

        private int ownerID;

        public int OwnerID
        {
            get
            {
                return ownerID;
            }
            set
            {
                ownerID = value;
                if (ownerID == 1)
                {
                    m_LockImageCanvasGroup.DOFade(0, 1).SetEase(Ease.InSine);
                    m_LockImageCanvasGroup.gameObject.SetActive(false);
                }
                else
                {
                    m_LockImageCanvasGroup.gameObject.SetActive(true);
                    m_LockImageCanvasGroup.DOFade(1, 1).SetEase(Ease.InSine);
                }
            }
        }

        private IStatisticsData ownerStatistics => OwnerID == 1 ? PlayerStatistics.Instance : null;

        private Problem currentProblem;

        public Problem CurrentProblem
        {
            get { return currentProblem; }
            set
            {
                currentProblem = value;

                if (currentProblem != null)
                {
                    currentProblem.Pos = SectorConfig.ProblemPoints[currentProblem.ProblemPosID];
                }
                if (OwnerID == 1)
                    m_WarningAnimator.SetBool("HasProblem", currentProblem != null);

            }
        }

        public double Production => ownerStatistics?.ProductionPerSec ?? m_SectorConfig.MinProduction;
        public double Rating => ownerStatistics?.Rating ?? m_SectorConfig.MinRating;
        public string OwnerName => OwnerID == 1 ? "Player" : "None";

        public void Setup(SectorData data, UIManager uiManager, Action onPlayButtonClickSound)
        {
            m_LockImageCanvasGroup.gameObject.SetActive(true);
            
            OwnerID = data?.OwnerID ?? 0;

            m_SectorName.text = m_SectorConfig.SectorName;
            
            m_SectorButton.onClick.AddListener(() =>
            {
                if (OwnerID != 1) return;

                onPlayButtonClickSound();
                uiManager.ViewController.Show<SectorView, SectorViewData>(new SectorViewData()
                {
                    Sector = this,
                },0.3f,0.3f);
            });

            m_AcquireButton.gameObject.SetActive(false);
            
            m_AcquireButton.onClick.AddListener(() =>
            {
                onPlayButtonClickSound();
                OwnerID = 1;
            });
            
            m_InfoButton.onClick.AddListener(() =>
            {
                uiManager.PopUpController.Show<InfoPopUp, InfoPopUpData>(GetInfoPopUpData());
            });
        }

        private void Update()
        {
            if (PlayerStatistics.Instance.Rating >= Rating &&
                PlayerStatistics.Instance.ProductionPerSec >= Production && OwnerID != 1)
                m_AcquireButton.gameObject.SetActive(true);
            else m_AcquireButton.gameObject.SetActive(false);

            if (CurrentProblem != null && OwnerID == 1)
                m_WarningAnimator.SetBool("HasProblem", true);
        }

        public SectorData CreateSectorData()
        {
            return new SectorData()
            {
                ID = SectorConfig.ID,
                OwnerID = this.OwnerID,
            };
        }

        private InfoPopUpData GetInfoPopUpData()
        {
            return new InfoPopUpData()
            {
                OwnerName = OwnerName,
                Name = m_SectorConfig.SectorName,
                Rating = Rating,
                ProductionPerSec = Production,
                ProfitPerSec = m_SectorConfig.ProfitPerSec
            };
        }
    }

    public class SectorData
    {
        public int ID;
        public int OwnerID;
    }
}