﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Mechanics.SectorSystem
{
    [CreateAssetMenu(fileName = "sectorConfig", menuName = "ScriptableObjects/SectorConfig", order = 0)]
    public class SectorConfig : ScriptableObject
    {
        [SerializeField] private Sprite m_SectorPlan;
        [SerializeField] private Sprite m_MaskPlan;
        [SerializeField] private int m_ID;
        [SerializeField] [Range(0,10f)] private double m_MinRating;
        [SerializeField] private double m_MinProduction;
        [SerializeField] private double m_ProfitPerSec;
        [SerializeField] private Vector2[] m_ProblemPoints = new Vector2[5];

        public Sprite SectorPlan => m_SectorPlan;
        public Sprite PlanMask => m_MaskPlan;
        public int ID => m_ID;
        public double MinRating => m_MinRating;
        public double MinProduction => m_MinProduction;
        public double ProfitPerSec => m_ProfitPerSec;
        public string SectorName => $"Sector{ID+1}";
        public Vector2[] ProblemPoints => m_ProblemPoints;
    }
}