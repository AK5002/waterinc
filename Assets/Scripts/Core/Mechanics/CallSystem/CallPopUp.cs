﻿using System;
using System.Collections;
using Core.Game;
using Core.Mechanics.ProblemSystem;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Mechanics.CallSystem
{
    public class CallPopUp : MonoBehaviour
    {
        [SerializeField] private CanvasGroup m_CanvasGroup;
        [SerializeField] private TMP_Text m_CallText;
        [SerializeField] private TMP_Text m_TimeLeft;
        [SerializeField] private Button m_AcceptButton;
        [SerializeField] private Button m_DeclineButton;

        public Problem Problem { get; private set; }
        private Action<CallPopUp> onHide;
        private int timeLeft;

        private Vector2 startAnchoredPos;

        public void Setup(Problem problem, Action<Problem> onAcceptButtonPressed, Action<Problem> onDeclineButtonPressed,Action<CallPopUp> onHide, Action onPlayButtonClickSound)
        {
            m_CanvasGroup.alpha = 0;
            startAnchoredPos = (transform as RectTransform).anchoredPosition;
            
            gameObject.SetActive(true);
            Problem = problem;
            
            m_DeclineButton.onClick.RemoveAllListeners();
            
            m_AcceptButton.onClick.AddListener(() =>
            {
                onPlayButtonClickSound();
                onAcceptButtonPressed(Problem);
                OnHide();
                m_AcceptButton.onClick.RemoveAllListeners();
            });
            m_DeclineButton.onClick.AddListener(() =>
            {
                onPlayButtonClickSound();
                onDeclineButtonPressed(Problem);
                OnHide();
                m_AcceptButton.onClick.RemoveAllListeners();
                m_DeclineButton.onClick.RemoveAllListeners();
            });

            this.onHide = onHide;
            timeLeft = 25;
            StartCoroutine(StartTimer(onDeclineButtonPressed));
        }
        
        public void OnShow()
        {
            m_CanvasGroup.alpha = 1;
            
            (transform as RectTransform).DOAnchorPos((transform as RectTransform).anchoredPosition + new Vector2(-490,0),1f)
                .SetEase(Ease.InSine);
            m_CallText.text = $"Hi! It seems that there is a problem in Sector {Problem.SectorID}";
        }

        private void OnHide()
        {
            Debug.Log("Hiding call");
            StopCoroutine(StartTimer());
            m_CanvasGroup.DOFade(0,0.5f)
                .SetEase(Ease.InSine).OnComplete(() =>
                {
                    onHide?.Invoke(this);
                    gameObject.SetActive(false);
                    (transform as RectTransform).anchoredPosition = startAnchoredPos;
                    Problem = null;
                });
        }

        private IEnumerator StartTimer(Action<Problem> onDecline = null)
        {
            while (true)
            {
                if (!GameplayController.GameplaySettings.IsGamePaused)
                {
                    timeLeft--;
                    m_TimeLeft.text = $"00:{timeLeft:00}";
                    if (timeLeft <= 0)
                    {
                        onDecline?.Invoke(Problem);
                        OnHide();
                        yield break;
                    }
                }
                yield return new WaitForSeconds(1f);
            }
        }
    }
}