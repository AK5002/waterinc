﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game;
using Core.Mechanics.ProblemSystem;
using Services.DependencyInjection;
using UnityEngine;

namespace Core.Mechanics.CallSystem
{
    public class CallController : MonoBehaviour
    {
        [SerializeField] private CallPopUp m_CallPopUp;

        private Queue<CallPopUp> queuedCalls;
        private List<CallPopUp> availablePopUps;
        private CallPopUp currentPopUp;
        
        public event Action<Problem> OnCallAccepted;
        public event Action<Problem> OnCallDeclined;

        private Action OnPlayButtonClickSound;
        
        public void Initialize(Action onPlayButtonClickSound)
        {
            queuedCalls = new Queue<CallPopUp>();
            availablePopUps = new List<CallPopUp>();

            OnPlayButtonClickSound = onPlayButtonClickSound;

            StartCoroutine(CheckForEmptySpace());
        }

        private IEnumerator CheckForEmptySpace()
        {
            while (true)
            {
                if(!GameplayController.GameplaySettings.IsGamePaused && !GameplayController.GameplaySettings.IsInSectorMapView)
                {
                    if(currentPopUp == null && queuedCalls.Count != 0)
                        AddCall(queuedCalls.Dequeue());
                }
                
                yield return new WaitForEndOfFrame();
            }
        }
        
        public void EnqueueCall(Problem problem)
        {
            var callPopUp = availablePopUps.FirstOrDefault();
            if (callPopUp == null)
            {
                callPopUp = Instantiate(m_CallPopUp, transform);
            }
            else
            {
                availablePopUps.Remove(callPopUp);
            } 
            
            callPopUp.Setup(problem,OnCallAccepted,OnCallDeclined, OnHide, OnPlayButtonClickSound);
            queuedCalls.Enqueue(callPopUp);
        }

        private void AddCall(CallPopUp callPopUp)
        {
            currentPopUp = callPopUp;
            callPopUp.OnShow();
        }

        private void OnHide(CallPopUp callPopUp)
        {
            availablePopUps.Add(callPopUp);
            currentPopUp = null;
        }
    }
}