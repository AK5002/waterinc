﻿using System;
using UnityEngine;

namespace Core.Mechanics.ProblemSystem
{
    public class Problem
    {
        public int ProblemPosID;
        public int SectorID;
        public int TimeLeft;

        public Vector2 Pos;

        public Action OnProblemSolved { get; private set; }
            
        public void Setup(Action onProblemSolved)
        {
            this.OnProblemSolved = onProblemSolved;
        }
    }

    public enum ProblemHardness
    {
        Easy = 30,
        Hard = 15,
    }
}