﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Mechanics.ProblemSystem
{
    public class ProblemGenerator
    {
        public event Action<Problem> OnProblemGenerated;

        public event Func<IEnumerable<int>> OnGetAvailableSectorIDs;
        public event Func<IEnumerable<int>> OnGetUnacceptedProblemSectorIDs;

        private readonly int timeInterval;

        public ProblemGenerator(int timeInterval)
        {
            this.timeInterval = timeInterval;
        }

        public IEnumerator GenerateProblemByRandom()
        {
            while (true)
            {
                if (!GameplayController.GameplaySettings.IsGamePaused &&
                    !GameplayController.GameplaySettings.IsInSectorMapView)
                {
                    int randomNumber = Random.Range(2, 9);
                    if (DateTime.Now.Second % randomNumber == 0 && OnGetAvailableSectorIDs?.Invoke().Count() != 0)
                    {
                        var problem = CreateProblem();
                        if(problem != null)
                            OnProblemGenerated?.Invoke(problem);
                        Debug.Log("Created Problem");
                    }
                }

                yield return new WaitForSeconds(timeInterval);
            }
        }

        private Problem CreateProblem()
        {
            var availableSectorIDs = OnGetAvailableSectorIDs?.Invoke();
            var unAcceptedProblemSectorIDs = OnGetUnacceptedProblemSectorIDs?.Invoke();
            

            var combinedList = availableSectorIDs.Except(unAcceptedProblemSectorIDs);

            var index = UnityEngine.Random.Range(0, combinedList.Count() - 1);
            try
            {
                Debug.LogWarning(index);
                Debug.LogWarning(combinedList.Count());
                var sectorID = combinedList.ToList()[index];
                return CreateProblemInSector(sectorID,false);
            }
            catch(ArgumentOutOfRangeException e)
            {
                Debug.Log(e.Source);
                return null;
            }
        }

        public Problem CreateProblemInSector(int sectorID, bool bypass)
        {
            var availableSectorIDs = OnGetAvailableSectorIDs?.Invoke();
            var unAcceptedProblemSectorIDs = OnGetUnacceptedProblemSectorIDs?.Invoke();

            Debug.Log(availableSectorIDs);
            Debug.Log(unAcceptedProblemSectorIDs);

            if (!bypass)
            {
                if (availableSectorIDs == null || unAcceptedProblemSectorIDs == null) return null;
                if (!availableSectorIDs.Contains(sectorID) && !unAcceptedProblemSectorIDs.Contains(sectorID)) return null;
            }

            ProblemHardness hardness =
                UnityEngine.Random.Range(0, 100) > 80 ? ProblemHardness.Hard : ProblemHardness.Easy;
            var problemPosID = UnityEngine.Random.Range(0, 4);

            var problem = new Problem() {ProblemPosID = problemPosID, SectorID = sectorID, TimeLeft = (int) hardness};

            return problem;
        }
    }
}