﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game;
using UnityEngine;

namespace Core.Mechanics.ProblemSystem
{
    public class ProblemController : MonoBehaviour
    {
        [SerializeField] private ProblemIcon m_ProblemIconPrefab;
        [SerializeField] private Transform m_IconHolder;
        [SerializeField] private int m_TimeInterval;

        private ProblemGenerator problemGenerator;
        private List<Problem> currentProblems;
        private List<Problem> unAcceptedProblems;

        public event Action<Problem> OnProblemGenerated;
        public event Action<Problem> OnProblemEnded;
        public event Action<Problem, int> OnProblemPass;

        public event Action<Problem> OnProblemSuccessful;
        public event Action<Problem> OnProblemUnsuccessful;

        public event Func<IEnumerable<int>> OnGetAvailableSectorIDs;

        private List<ProblemIcon> icons;

        public void Setup()
        {
            currentProblems = new List<Problem>();
            unAcceptedProblems = new List<Problem>();
            icons = new List<ProblemIcon>();

            problemGenerator = new ProblemGenerator(m_TimeInterval);
            problemGenerator.OnProblemGenerated += (problem) =>
            {
                unAcceptedProblems.Add(problem);
                OnProblemGenerated?.Invoke(problem);
            };
            problemGenerator.OnGetAvailableSectorIDs += OnGetAvailableSectorIDs;
            problemGenerator.OnGetUnacceptedProblemSectorIDs +=
                () => unAcceptedProblems.Select(problem => problem.SectorID);

            StartCoroutine(problemGenerator.GenerateProblemByRandom());
        }

        public void AddProblem(Problem problem)
        {
            DeleteUnacceptedProblem(problem);
            currentProblems.Add(problem);
            problem.Setup(() =>
            {
                OnProblemSuccessful?.Invoke(problem);
                RemoveProblem(problem);
            });
            OnProblemPass?.Invoke(problem, problem.SectorID);
            if (currentProblems.Count == 1)
                StartCoroutine(ProblemTimer());
        }

        private void RemoveProblem(Problem problem)
        {
            if (currentProblems.Contains(problem))
            {
                OnProblemEnded?.Invoke(problem);
                currentProblems.Remove(problem);
            }

            if (currentProblems.Count == 0)
                StopCoroutine(ProblemTimer());

            var problemIcon = icons.FirstOrDefault(icon => icon.Problem == problem);
            if (problemIcon != null)
                problemIcon.OnHide();
        }

        public void DeleteUnacceptedProblem(Problem problem)
        {
            unAcceptedProblems.Remove(problem);
        }

        public void AddProblemIcon(Problem problem)
        {
            var problemIcon = icons.FirstOrDefault(icon => icon.IsAvailable);
            if (problemIcon == null)
            {
                problemIcon = Instantiate(m_ProblemIconPrefab, m_IconHolder);
                icons.Add(problemIcon);
            }

            problemIcon.OnShow(problem);
        }

        private IEnumerator ProblemTimer()
        {
            while (true)
            {
                if (!GameplayController.GameplaySettings.IsGamePaused)
                {
                    if (currentProblems.Count == 0) yield break;

                    foreach (var problem in currentProblems.ToList())
                    {
                        problem.TimeLeft--;
                        if (problem.TimeLeft <= 0)
                        {
                            OnProblemUnsuccessful?.Invoke(problem);
                            RemoveProblem(problem);
                        }
                    }
                }

                yield return new WaitForSeconds(1f);
            }
        }
        
        public void CreateProblemInPlayerSector()
        {
            var problem = problemGenerator.CreateProblemInSector(0,true);
            unAcceptedProblems.Add(problem);
            OnProblemGenerated?.Invoke(problem);
        }
    }
}