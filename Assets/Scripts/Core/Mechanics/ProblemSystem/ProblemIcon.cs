﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Core.Mechanics.ProblemSystem
{
    public class ProblemIcon : MonoBehaviour
    {
        [SerializeField] private CanvasGroup m_CanvasGroup;
        [SerializeField] private TMP_Text m_Timer;

        public Problem Problem { get; private set; }
        public bool IsAvailable { get; private set; }

        public void OnShow(Problem problem)
        {
            Problem = problem;
            IsAvailable = false;
            
            m_CanvasGroup.alpha = 0;
            gameObject.SetActive(true);
            m_CanvasGroup.DOFade(1, 1f);
        }

        private void Update()
        {
            m_Timer.text = $"00:{Problem.TimeLeft:00}";
        }

        public void OnHide()
        {
            IsAvailable = true;
            gameObject.SetActive(false);
        }
    }
}