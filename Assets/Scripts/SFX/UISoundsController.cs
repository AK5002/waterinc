﻿using System;
using UnityEngine;

namespace SFX
{
    public class UISoundsController : MonoBehaviour
    {
        [SerializeField] private AudioClip m_ButtonClickSound;
        [SerializeField] private AudioClip m_ProblemSolveSound;

        public Action OnPlayButtonClickSound { get; private set; }
        public Action OnProblemSolveSound { get; private set; }

        public void Initialize(AudioController audioController)
        {
            OnPlayButtonClickSound = () => audioController.PlayClip(AudioGroup.Effects, m_ButtonClickSound);
            OnProblemSolveSound = () => audioController.PlayClip(AudioGroup.Effects, m_ProblemSolveSound);
        }
    }
}