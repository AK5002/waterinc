﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SFX
{
    public class BackgroundMusicController : MonoBehaviour
    {
        [SerializeField] private List<BackgroundMusicClip> m_Clips;
        [SerializeField] private AudioSource m_Source;
        
        private int CurrentIndex;

        public void StartPlay()
        {
            m_Source.Stop();
            CurrentIndex = 0;
            m_Source.clip = m_Clips.Find(clipToFind => clipToFind.Order == CurrentIndex).Clip;
            m_Source.Play();
        }

        public void StartPlay(int fromIndex)
        {
            m_Source.Stop();
            CurrentIndex = fromIndex;
            var clip = m_Clips.FirstOrDefault(clipToFind => clipToFind.Order == CurrentIndex)?.Clip;
            if (clip == null) return;
            m_Source.clip = clip;
            m_Source.Play();
        }

        private void Update()
        {
            if (!m_Source.isPlaying)
            {
                if (CurrentIndex < m_Clips.Count - 1)
                    CurrentIndex++;
                else CurrentIndex = 0;
                StartPlay(CurrentIndex);
            }
        }
    }

    [Serializable]
    internal class BackgroundMusicClip
    {
        public AudioClip Clip;
        public int Order;
    }

}
