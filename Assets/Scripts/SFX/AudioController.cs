﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace SFX
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private AudioMixer m_Mixer;
        
        [SerializeField] private AudioMixerGroup m_EffectsGroup;
        [SerializeField] private AudioMixerGroup m_MusicGroup;

        [SerializeField] private GameObject m_AudioSourceContainer;

        private List<AudioSource> availableSources;
        private List<AudioSource> currentSources;

        public void Initialize()
        {
            availableSources = new List<AudioSource>();
            currentSources = new List<AudioSource>();

            StartCoroutine(CheckCurrentSources());
        }

        public void SetVolume(AudioGroup group, float volume)
        {
            string paramName;

            switch (group)
            {
                case AudioGroup.Effects:
                    paramName = "EffectsVolume";
                    break;
                case AudioGroup.Music:
                    paramName = "MusicVolume";
                    break;
                default:
                    paramName = "MasterVolume";
                    break;
            }

            m_Mixer.SetFloat(paramName, volume);
        }

        public void SetVolume(AudioGroup group, bool on)
        {
            string paramName;

            switch (group)
            {
                case AudioGroup.Effects:
                    paramName = "EffectsVolume";
                    break;
                case AudioGroup.Music:
                    paramName = "MusicVolume";
                    break;
                default:
                    paramName = "MasterVolume";
                    break;
            }
 
            m_Mixer.SetFloat(paramName, on ? 0 : -80f);
        }

        public void PlayClip(AudioGroup group, AudioClip clip)
        {
            var source = availableSources.FirstOrDefault();

            if (source != null)
                availableSources.Remove(source);
            else source = m_AudioSourceContainer.AddComponent<AudioSource>();

            switch (group)
            {
                case AudioGroup.Effects:
                    source.outputAudioMixerGroup = m_EffectsGroup;
                    break;
                case  AudioGroup.Music:
                    source.outputAudioMixerGroup= m_MusicGroup;
                    break;
            }
            source.clip = clip;
            currentSources.Add(source);
            source.Play();
        }

        private IEnumerator CheckCurrentSources()
        {
            while (true)
            {
                foreach (var source in currentSources.ToList())
                {
                    if(source.isPlaying) continue;

                    currentSources.Remove(source);
                    availableSources.Add(source);
                }
                
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public enum AudioGroup
    {
        Master,
        Effects,
        Music,
    }
}