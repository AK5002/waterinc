﻿using System.Collections.Generic;
using System.Linq;
using Managers;
using Services.DependencyInjection;
using UnityEngine;

namespace UI
{
    public class PopUpController : AbstractPresenter
    {
        public PopUpMenu CurrentPopUp { get; private set; }
        private readonly List<PopUpMenu> popUpPrefabs;
        private readonly List<PopUpMenu> popUps;
        private readonly Transform popUpContainer;

        public PopUpController(UIManager uiManager, List<PopUpMenu> popUpPrefabs, Transform popUpContainer)
        {
            UIManager = uiManager;
            this.popUpPrefabs = popUpPrefabs;
            this.popUpContainer = popUpContainer;
            popUps = new List<PopUpMenu>();

            popUpContainer.gameObject.SetActive(false);
        }

        public T Show<T, K>(K data) where T : PopUpMenu where K : UIData
        {
            if (CurrentPopUp != null)
                Hide(CurrentPopUp);

            var menu = popUps.FirstOrDefault(popUp => popUp.Group == typeof(T));

            if (menu != null && (menu.transform as RectTransform).anchoredPosition.y != -2000)
            {
                return null;
            }
            
            if (menu == null)
            {
                menu = popUpPrefabs.FirstOrDefault(popUp => popUp.Group == typeof(T));
                if (menu == null)
                {
                    Debug.Log("Could not find menu");
                    return null;
                }

                menu = Object.Instantiate(menu, popUpContainer);
                popUps.Add(menu);
            }
            
            CurrentPopUp = menu;
            popUpContainer.gameObject.SetActive(true);

            menu.gameObject.SetActive(true);
            (menu.transform as RectTransform).anchoredPosition = new Vector2(0, -2000f);
            menu.Setup(data, this);
            menu.OnShow();
            UIManager.OnPlayButtonClickSound();

            return (T) menu;
        }

        public void Hide<T>(T menuToHide) where T : PopUpMenu
        {
            popUpContainer.gameObject.SetActive(false);
            CurrentPopUp = null;
            menuToHide.OnHide();
        }
    }
}