﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public abstract class PopUpMenu : MonoBehaviour
    {
        [SerializeField] private RectTransform m_RectTransform;
        [SerializeField] protected CanvasGroup m_CanvasGroup;

        [SerializeField] protected Button m_CloseButton;
        protected PopUpController popUpController { get; set; }
        
        public abstract Type Group { get; }

        public virtual void Setup(UIData data, PopUpController popUpController)
        {
            this.popUpController = popUpController;
            
            m_CloseButton.onClick.AddListener(() =>
            {
                popUpController.UIManager.OnPlayButtonClickSound();
                popUpController.Hide(this);
            });
        }

        public virtual void OnShow()
        {
            m_RectTransform.DOJumpAnchorPos(new Vector2(0,0),100,1,1f).
                OnComplete(() => gameObject.SetActive(true));
        }
        
        public virtual void OnHide()
        {
            m_RectTransform.DOJumpAnchorPos(new Vector2(0,-2000),-100,1,1f).
                OnComplete(() => gameObject.SetActive(false));
        }
    }
}