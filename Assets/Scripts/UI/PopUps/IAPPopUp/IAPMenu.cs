﻿using System;
using System.Collections.Generic;
using Core.Game.Bank;
using UnityEngine;

namespace UI.PopUps.IAPPopUp
{
    public class IAPMenu : PopUpMenu
    {
        [SerializeField] private List<IAPCell> m_Cells;
        public override Type Group => typeof(IAPMenu);
        
        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);

            foreach (var cell in m_Cells)
            {
                cell.Setup(PlayerBank.Instance.AddAmount);        
            }
        }
    }
}