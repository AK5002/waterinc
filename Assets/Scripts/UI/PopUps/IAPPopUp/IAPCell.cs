﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace UI.PopUps.IAPPopUp
{
    public class IAPCell : MonoBehaviour
    {
        [SerializeField] private IAPButton m_PurchaseButton;

        public void Setup(Action<double> onButtonClick)
        {
            m_PurchaseButton.onPurchaseComplete.RemoveAllListeners();
            m_PurchaseButton.onPurchaseComplete.AddListener((product) =>
            {
                Debug.Log($"Purchased a product{product.definition.id} which {product.metadata.localizedDescription} of price {product.metadata.localizedPrice}," +
                          $" with a reward of type {product.definition.payout.subtype}, in quantity of {product.definition.payout.quantity}");
                onButtonClick?.Invoke(product.definition.payout.quantity);
            });
            
            m_PurchaseButton.onPurchaseFailed.RemoveAllListeners();
            m_PurchaseButton.onPurchaseFailed.AddListener((product,failReason) =>
            {
                Debug.Log($"Failed to purchase a product{product.definition.id} which {product.metadata.localizedDescription} of price {product.metadata.localizedPrice}," +
                          $" with a reward of type {product.definition.payout.subtype}, in quantity of {product.definition.payout.quantity}. Reason: {failReason}");
            });
        }
    }
}