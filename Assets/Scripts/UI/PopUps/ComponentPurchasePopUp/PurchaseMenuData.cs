﻿using System;
using System.Collections.Generic;
using Core.Game.ProductionLine.Config;

namespace UI.PopUps.ComponentPurchasePopUp
{
    public class PurchaseMenuData : UIData
    {
        public List<ComponentConfig> Configs;
        public HashSet<int> AvailableConfigIDs;
        public ComponentConfig CurrentConfig;

        public Func<ComponentConfig, bool, bool> OnButtonPressed;
        public Func<PurchaseMenuData> OnDataUpdate;

        public int ConveyorID;
    }
}