﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.ComponentPurchasePopUp
{
    public class PurchasePopUp : PopUpMenu,IPurchaseMenu
    {
        [SerializeField] private List<MenuCell> m_Cells;
        [SerializeField] private MenuCell m_Cell;
        [SerializeField] private Transform m_CellHolder;

        public override Type Group => typeof(PurchasePopUp);
        private PurchaseMenuData Data;

        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            Data = (PurchaseMenuData) data;
        }

        public override void OnShow()
        {
            base.OnShow();
            UpdateMenu();
        }

        public override void OnHide()
        {
            base.OnHide();
            
            foreach (var cell in m_Cells)
                cell.IsAvailable = true;
        }

        public void UpdateMenu()
        {
            var newData = Data.OnDataUpdate();

            Data.CurrentConfig = newData.CurrentConfig;
            Data.AvailableConfigIDs = newData.AvailableConfigIDs;
            
            Debug.Log(Data);
            foreach (var cell in m_Cells)
                cell.IsAvailable = true;
            
            foreach (var config in Data.Configs.OrderBy(config => config.ID))
            {
                var cell = m_Cells.FirstOrDefault(cellToFind => cellToFind.IsAvailable) ?? Instantiate(m_Cell, m_CellHolder);
                var strategy = new BuyButtonStrategy(this, (componentConfig) =>
                {
                    popUpController.UIManager.OnPlayButtonClickSound();
                    return Data.OnButtonPressed(componentConfig, true);
                }) as CellButtonStrategy;
                if (Data.CurrentConfig.ID == config.ID)
                    strategy = null;
                else if (Data.AvailableConfigIDs.Any(id => id == config.ID))
                    strategy = new SetButtonStrategy(this, (componentConfig) =>
                    {
                        popUpController.UIManager.OnPlayButtonClickSound();
                        return Data.OnButtonPressed(componentConfig, false);
                    });

                cell.Setup(config,strategy);
                if(!m_Cells.Contains(cell))
                    m_Cells.Add(cell);
            }
        }
    }

    public interface IPurchaseMenu
    {
        void UpdateMenu();
    }
}