﻿using System;
using Core.Game.ProductionLine.Config;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Utilities.UI;

namespace UI.PopUps.ComponentPurchasePopUp
{
    public class MenuCell : MonoBehaviour
    {
        [SerializeField] private CanvasGroup m_Group;
        [SerializeField] private Image m_Icon;
        [SerializeField] private TMP_Text m_ComponentName;
        [SerializeField] private TMP_Text m_ProductionText;
        [SerializeField] private TMP_Text m_CostText;
        [SerializeField] private TMP_Text m_Price;
        [SerializeField] private Button m_Button;
        
        [SerializeField] private Image m_ButtonImage;
        [SerializeField] private Sprite m_BuyImage;
        [SerializeField] private Sprite m_SetImage;
        [SerializeField] private Sprite m_NoneImage;

        public bool IsAvailable = true;

        public void Setup(ComponentConfig config,CellButtonStrategy strategy)
        {
            IsAvailable = false;
            
            m_Group.alpha = 0;
            m_Icon.sprite = config.Icon;
            m_ComponentName.text = config.ComponentName;
            m_ProductionText.text = $"{UIUtils.NumberToDecimalNotation(config.ProductionPerSec.ToString(),0)} L/SEC";
            m_CostText.text = $"{UIUtils.NumberToDecimalNotation(config.CostPerSec.ToString(),0) } W";
            m_Price.text = $"{UIUtils.NumberToDecimalNotation(config.Price.ToString(),0)} W";
            
            m_Button.onClick.RemoveAllListeners();
            if (strategy == null)
            {
                m_ButtonImage.sprite = m_NoneImage;
            }
            else if (strategy.GetType() == typeof(BuyButtonStrategy))
            {
                m_ButtonImage.sprite = m_BuyImage;
                m_Button.onClick.AddListener(() => strategy.DoAlgorithm(config));
            }
            else
            {
                m_ButtonImage.sprite = m_SetImage;
                m_Button.onClick.AddListener(() => strategy.DoAlgorithm(config));
            }
            m_Group.DOFade(1, 1f).SetEase(Ease.InSine);
        }
    }

    public abstract class CellButtonStrategy
    {
        protected readonly IPurchaseMenu menu;
        protected readonly Func<ComponentConfig,bool> onPress;
        public abstract string Action { get; }

        public abstract void DoAlgorithm(ComponentConfig config);
        
        public CellButtonStrategy(IPurchaseMenu menu, Func<ComponentConfig,bool> onPress)
        {
            this.menu = menu;
            this.onPress = onPress;
        }
    }
    
    public class BuyButtonStrategy: CellButtonStrategy
    {
        public override string Action => "Buy";
        
        public BuyButtonStrategy(IPurchaseMenu menu, Func<ComponentConfig,bool> onPress) : base(menu, onPress)
        {
        }

        public override void DoAlgorithm(ComponentConfig config)
        {
            if(onPress(config))
                menu.UpdateMenu();
        }

        
    }
    
    public class SetButtonStrategy : CellButtonStrategy
    {
        public override string Action => "Set";
        
        public SetButtonStrategy(IPurchaseMenu menu, Func<ComponentConfig,bool> onPress) : base(menu, onPress)
        {
        }

        public override void DoAlgorithm(ComponentConfig config)
        {
            if(onPress(config))
                menu.UpdateMenu();
        }
    }
}