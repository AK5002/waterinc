﻿using System;
using Core.Game;
using SFX;

namespace UI.PopUps.SettingsMenu
{
    public class SettingsMenuData : UIData
    {

        public Action<AudioGroup, bool> OnValueChanged;
    }
}