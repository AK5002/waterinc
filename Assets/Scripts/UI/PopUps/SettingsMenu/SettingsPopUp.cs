﻿using System;
using Core.Game;
using Managers;
using SFX;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.SettingsMenu
{
    public class SettingsPopUp : PopUpMenu
    {
        [SerializeField] private Toggle m_EffectsVolumeToggle;
        [SerializeField] private Toggle m_MusicVolumeToggle;
        
        public override Type Group => typeof(SettingsPopUp);
        private SettingsMenuData data;

        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            this.data = (SettingsMenuData) data;

            m_EffectsVolumeToggle.isOn = GameManager.Settings.IsEffectsOn;
            m_MusicVolumeToggle.isOn = GameManager.Settings.IsMusicOn;
            
            m_EffectsVolumeToggle.onValueChanged.RemoveAllListeners();
            m_EffectsVolumeToggle.onValueChanged.AddListener((value) =>
            {
                popUpController.UIManager.OnPlayButtonClickSound();
                Debug.Log("Effects Volume change");
                this.data.OnValueChanged?.Invoke(AudioGroup.Effects, value);
            });
            
            m_MusicVolumeToggle.onValueChanged.RemoveAllListeners();
            m_MusicVolumeToggle.onValueChanged.AddListener((value) =>
            {
                popUpController.UIManager.OnPlayButtonClickSound();
                Debug.Log("Music Volume change");
                this.data.OnValueChanged?.Invoke(AudioGroup.Music, value);
            });
        }
    }
}