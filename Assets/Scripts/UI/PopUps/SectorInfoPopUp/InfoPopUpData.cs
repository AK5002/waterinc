﻿using Core.Mechanics.SectorSystem;

namespace UI.PopUps.SectorInfoPopUp
{
    public class InfoPopUpData : UIData
    {
        public string Name;
        public double Rating;
        public double ProductionPerSec;
        public double ProfitPerSec;
        public string OwnerName;
    }
}