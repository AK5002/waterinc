﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.UI;

namespace UI.PopUps.SectorInfoPopUp
{
    public class InfoPopUp : PopUpMenu
    {
        [SerializeField] private TMP_Text m_SectorName;
        [SerializeField] private TMP_Text m_Rating;
        [SerializeField] private TMP_Text m_Production;
        [SerializeField] private TMP_Text m_Profit;
        [SerializeField] private TMP_Text m_Owner;

        public override Type Group => typeof(InfoPopUp);

        private InfoPopUpData data;

        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            this.data = (InfoPopUpData) data;
        }

        public override void OnShow()
        {
            base.OnShow();

            m_SectorName.text = data.Name;
            m_Production.text = $"{UIUtils.NumberToDecimalNotation(data.ProductionPerSec.ToString(),2)} L/SEC";
            m_Profit.text = $"{UIUtils.NumberToDecimalNotation(data.ProfitPerSec.ToString(),2)} W/SEC";
            m_Rating.text = $"{data.Rating:F1}";
            m_Owner.text = "OWNER: " + data.OwnerName;
        }
    }
}