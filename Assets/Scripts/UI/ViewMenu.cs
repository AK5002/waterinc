﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace UI
{
    public abstract class ViewMenu : MonoBehaviour
    {
        [SerializeField] protected CanvasGroup m_CanvasGroup;
        [SerializeField] private List<RectTransform> SafeAreaRectTransforms;
        protected ViewController ViewController { get; set; }


        public abstract Type Group { get; }


        public virtual void Setup(UIData data, ViewController viewController)
        {
            ViewController = viewController;

            foreach (var rectTransform in SafeAreaRectTransforms)
            {
                rectTransform.FitInSafeArea();
            }
        }

        public abstract void OnShow(float fadeOutTime);

        public virtual void OnHide(float fadeInTime)
        {
        
        }
    }
}