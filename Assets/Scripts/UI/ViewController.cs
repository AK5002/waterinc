﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using Managers;
using Services.DependencyInjection;
using SFX;
using UI.Views;
using UI.Views.FactoryMenu;
using UI.Views.SectorViewMenu;
using UnityEngine;
using Utilities.UI;
using Object = UnityEngine.Object;

namespace UI
{
    public class ViewController : AbstractPresenter
    {
        public ViewMenu CurrentView { get; private set; }

        private readonly Canvas canvas;

        private readonly List<ViewMenu> viewPrefabs;
        private readonly Transform viewContainer;

        private readonly ViewMenu factoryView;
        private readonly ViewMenu sectorMapView;

        private readonly PageSwiper pageSwiper;

        public ViewController(UIManager uiManager, List<ViewMenu> viewPrefabs, Transform viewContainer,
            FactoryView factoryView, SectorMapView sectorMapView, PageSwiper pageSwiper, Canvas canvas)
        {
            UIManager = uiManager;
            this.canvas = canvas;
            this.viewPrefabs = viewPrefabs;
            this.viewContainer = viewContainer;
            this.factoryView = factoryView;
            this.sectorMapView = sectorMapView;
            this.pageSwiper = pageSwiper;
            this.pageSwiper.OnSwipeComplete += (direction) =>
            {
                if (this.pageSwiper.PageIndex == 0)
                    Show<FactoryView, UIData>(null, 0.3f, 0.3f);
                else Show<SectorMapView, UIData>(null, 0.3f, 0.3f);
            };
        }

        public T Show<T, K>(K data, float fadeinTime, float fadeoutTime) where T : ViewMenu where K : UIData
        {
            if (CurrentView != null)
            {
                Hide(CurrentView, fadeinTime);
                if (UIManager.PopUpController.CurrentPopUp != null)
                    UIManager.PopUpController.Hide(UIManager.PopUpController.CurrentPopUp);
            }

            ViewMenu menu;
            if (typeof(T) == typeof(FactoryView) || typeof(T) == typeof(SectorMapView))
            {
                menu = typeof(T) == typeof(FactoryView) ? factoryView : sectorMapView;

                var otherMenu = typeof(T) == typeof(FactoryView) ? sectorMapView : factoryView;
                otherMenu.OnHide(fadeinTime);

                menu.gameObject.SetActive(true);
            }
            else
            {
                menu = viewPrefabs.FirstOrDefault(view => view.Group == typeof(T));
                menu = Object.Instantiate(menu, viewContainer);

                if (menu is SectorView)
                    ((RectTransform) menu.transform).anchoredPosition =
                        new Vector2(Screen.width / canvas.scaleFactor, 0);
            }

            menu.Setup(data, this);
            menu.OnShow(fadeoutTime);

            CurrentView = menu;

            return (T) menu;
        }

        public void Hide<T>(T menuToHide, float fadeInTime) where T : ViewMenu
        {
            Debug.Log("Hiding View...");
            menuToHide.OnHide(fadeInTime);
        }
    }
}