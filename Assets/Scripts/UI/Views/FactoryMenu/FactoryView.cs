﻿using System;
using System.Collections;
using Core.Game.Statistics;
using DG.Tweening;
using TMPro;
using UI.PopUps.IAPPopUp;
using UI.PopUps.SettingsMenu;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utilities.UI;

namespace UI.Views.FactoryMenu
{
    public class FactoryView : ViewMenu
    {
        [SerializeField] private Button m_IAPMenuButton;
        [SerializeField] private Button m_SettingsMenuButton;

        [SerializeField] private TMP_Text m_Money;
        [SerializeField] private TMP_Text m_ProfitPerSec;
        [SerializeField] private TMP_Text m_ProductionPerSec;
        [SerializeField] private TMP_Text m_Rating;
        [SerializeField] private TMP_Text m_CustomerServiceRating;
        [SerializeField] private TMP_Text m_WaterQualityRating;

        public override Type Group => typeof(FactoryView);

        public override void Setup(UIData data, ViewController viewController)
        {
            base.Setup(data, viewController);

            m_IAPMenuButton.onClick.RemoveAllListeners();
            m_IAPMenuButton.onClick.AddListener(() =>
            {
                viewController.UIManager.PopUpController.Show<IAPMenu, UIData>(null);
            });

            m_SettingsMenuButton.onClick.RemoveAllListeners();
            m_SettingsMenuButton.onClick.AddListener(() =>
            {
                viewController.UIManager.PopUpController.Show<SettingsPopUp, SettingsMenuData>(
                    new SettingsMenuData() {OnValueChanged = viewController.UIManager.OnSettingsButtonPressed});
            });
        }

        public override void OnShow(float fadeOutTime)
        {
            m_CanvasGroup.alpha = 0;
            m_CanvasGroup.DOFade(1, fadeOutTime).SetEase(Ease.InSine);
            StartCoroutine(UpdateView());
        }

        private IEnumerator UpdateView()
        {
            while (true)
            {
                m_Money.text = $"{UIUtils.NumberToDecimalNotation(PlayerStatistics.Instance.Money.ToString(),2)} W";
                m_ProfitPerSec.text =
                    $"{UIUtils.NumberToDecimalNotation(PlayerStatistics.Instance.ProfitPerSec.ToString(),2)}/SEC W";
                m_ProductionPerSec.text =
                    $"{UIUtils.NumberToDecimalNotation(PlayerStatistics.Instance.ProductionPerSec.ToString(),2)} L/SEC";
                m_Rating.text = $"{PlayerStatistics.Instance.Rating:F1}/10.0";
                m_CustomerServiceRating.text = $"{PlayerStatistics.Instance.CustomerServiceRating:F1}/5.0";
                m_WaterQualityRating.text = $"{PlayerStatistics.Instance.WaterQualityRating:F1}/5.0";

                yield return new WaitForSeconds(1f);
            }
        }

        public override void OnHide(float fadeInTime)
        {
            m_CanvasGroup.DOFade(0, fadeInTime).SetEase(Ease.InSine);
            StopCoroutine(UpdateView());
        }
    }
}