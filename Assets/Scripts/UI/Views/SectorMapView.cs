﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UI.Views
{
    public class SectorMapView : ViewMenu
    {
        public override Type Group => typeof(SectorMapView);
        public override void OnShow(float fadeOutTime)
        {
            m_CanvasGroup.alpha = 0;
            m_CanvasGroup.DOFade(1, fadeOutTime).SetEase(Ease.InSine);
        }

        public override void OnHide(float fadeInTime)
        {
            m_CanvasGroup.DOFade(0, fadeInTime).SetEase(Ease.InSine);
        }
    }
}