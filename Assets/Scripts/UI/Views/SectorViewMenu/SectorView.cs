﻿using System;
using System.Collections.Generic;
using Core.Game.Statistics;
using DG.Tweening;
using TMPro;
using UI.PopUps.SectorInfoPopUp;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.SectorViewMenu
{
    public class SectorView : ViewMenu
    {
        [SerializeField] private Image m_SectorMask;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Button m_InfoButton;
        [SerializeField] private TMP_Text m_SectorName;

        [SerializeField] private Button m_ProblemPointPrefab;

        [SerializeField] private GameObject m_CleanWaterContainer;
        [SerializeField] private GameObject m_DirtyWaterContainer;

        private SectorViewData data;
        private Button problemPoint;

        public override Type Group => typeof(SectorView);

        public override void Setup(UIData data, ViewController viewController)
        {
            base.Setup(data, viewController);
            this.data = (SectorViewData) data;

            m_CanvasGroup.alpha = 0;
            var isWaterClean = PlayerStatistics.Instance.CleanWaterPercentage > 50;
            m_CleanWaterContainer.SetActive(isWaterClean);
            m_DirtyWaterContainer.SetActive(!isWaterClean);
            m_SectorMask.sprite = this.data.Sector.SectorConfig.PlanMask;
        }

        public override void OnShow(float fadeOutTime)
        {
            m_CanvasGroup.DOFade(1f, fadeOutTime).SetEase(Ease.InSine).SetDelay(0.3f);
            
            m_BackButton.onClick.AddListener(() =>
            {
                ViewController.UIManager.OnPlayButtonClickSound();
                ViewController.Show<SectorMapView, UIData>(null, 0.3f, 0.3f);
            });

            m_InfoButton.onClick.RemoveAllListeners();
            m_InfoButton.onClick.AddListener(() =>
            {
                ViewController.UIManager.PopUpController.Show<InfoPopUp, InfoPopUpData>(new InfoPopUpData()
                {
                    Name = data.Sector.SectorConfig.SectorName,
                    OwnerName = data.Sector.OwnerName,
                    ProductionPerSec = data.Sector.Production,
                    ProfitPerSec = data.Sector.SectorConfig.ProfitPerSec,
                    Rating = data.Sector.Rating,
                });
            });

            m_SectorName.text = data.Sector.SectorConfig.SectorName;
        }

        private void Update()
        {
            if (data.Sector.CurrentProblem != null && problemPoint == null)
            {
                problemPoint = Instantiate(m_ProblemPointPrefab, transform);
                ((RectTransform) problemPoint.transform).anchoredPosition = data.Sector.CurrentProblem.Pos;
                problemPoint.onClick.AddListener(() =>
                {
                    ViewController.UIManager.OnProblemSolveSound();
                    Debug.Log(data.Sector.CurrentProblem);
                    data.Sector.CurrentProblem?.OnProblemSolved?.Invoke();
                    Destroy(problemPoint.gameObject);
                    problemPoint = null;
                });
            }
        }

        public override void OnHide(float fadeInTime)
        {
            base.OnHide(fadeInTime);
            Destroy(gameObject);
        }
    }
}