﻿using Core.Mechanics.SectorSystem;

namespace UI.Views.SectorViewMenu
{
    public class SectorViewData : UIData
    {
        public Sector Sector;
    }
}