﻿using System;
using System.Collections;
using Core.Game;
using Core.Game.Bank;
using Services.DependencyInjection;
using Services.SaveService;
using SFX;
using UI.Views.FactoryMenu;
using UnityEngine;
using UnityEngine.Networking;

namespace Managers
{
    public class GameManager : MonoBehaviour, ISettings
    {
        [SerializeField] private UIManager m_UIManager;
        [SerializeField] private GameplayController m_GameplayController;
        [SerializeField] private AudioController m_AudioController;
        [SerializeField] private BackgroundMusicController m_BGMusicController;
        [SerializeField] private UISoundsController m_UISoundsController;
        [SerializeField] private Injector m_Injector;


        private SaveManager saveManager;

        public static ISettings Settings;

        private bool isSoundOn;
        private bool isMusicOn;
        private bool isEffectsOn;

        public bool IsSoundOn
        {
            get => isSoundOn;
            set
            {
                isSoundOn = value;
                m_AudioController.SetVolume(AudioGroup.Master, value);
            }
        }

        public bool IsEffectsOn
        {
            get => isEffectsOn;
            set
            {
                isEffectsOn = value;
                m_AudioController.SetVolume(AudioGroup.Effects, value);
            }
        }

        public bool IsMusicOn
        {
            get => isMusicOn;
            set
            {
                isMusicOn = value;
                m_AudioController.SetVolume(AudioGroup.Music, value);
            }
        }

        private void Start()
        {
            Initialize();
            Setup();
        }

        private void Initialize()
        {
            Application.targetFrameRate = 60;

            saveManager = new SaveManager();
            saveManager.Initialize();

            isSoundOn = saveManager.CurrentSettings.IsSoundOn;
            isEffectsOn = saveManager.CurrentSettings.IsEffectsOn;
            isMusicOn = saveManager.CurrentSettings.IsMusicOn;
            Settings = this;

            m_AudioController.Initialize();
            m_UISoundsController.Initialize(m_AudioController);
            m_UIManager.Initialize(m_UISoundsController);
            m_GameplayController.Initialize(saveManager, m_UIManager, m_UISoundsController);

            StartCoroutine(AudioInitialize());
        }

        private void Setup()
        {
            m_UIManager.OnSettingsButtonPressed = (group, value) =>
            {
                switch (group)
                {
                    case AudioGroup.Master:
                        IsSoundOn = value;
                        break;
                    case AudioGroup.Effects:
                        IsEffectsOn = value;
                        break;
                    case AudioGroup.Music:
                        IsMusicOn = value;
                        break;
                }
            };

            m_GameplayController.Setup();

            m_BGMusicController.StartPlay();

            m_GameplayController.OnFactoryViewEnter();
        }

        private IEnumerator AudioInitialize()
        {
            m_AudioController.SetVolume(AudioGroup.Music, IsMusicOn);

            yield return new WaitForEndOfFrame();

            m_AudioController.SetVolume(AudioGroup.Master, IsSoundOn);

            yield return new WaitForEndOfFrame();

            m_AudioController.SetVolume(AudioGroup.Effects, IsEffectsOn);
        }

#if UNITY_EDITOR
        private void OnApplicationQuit()
        {
            Debug.Log("save");
            var (problemManager, conveyorController, sectorController) = m_GameplayController;
            saveManager?.SaveGame(PlayerBank.Instance.Money, problemManager.PlayerCustomerServiceRating,
                conveyorController.CreateSaveDataList(), sectorController.CreateSectorDataList());
            saveManager?.SaveSettings(IsSoundOn,IsMusicOn,IsEffectsOn);
        }
#else
        private void OnApplicationPause(bool pauseStatus)
        {
            if(!pauseStatus) return;

             Debug.Log("save");
            var (problemManager, conveyorController, sectorController) = m_GameplayController;
            saveManager?.SaveGame(PlayerBank.Instance.Money, problemManager.PlayerCustomerServiceRating,
                conveyorController.CreateSaveDataList(),sectorController.CreateSectorDataList());
            saveManager?.SaveSettings(IsSoundOn,IsMusicOn,IsEffectsOn);
        }

#endif
    }
}