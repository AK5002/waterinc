﻿using System;
using System.Collections.Generic;
using Core.Mechanics.SectorSystem;
using SFX;
using UI;
using UI.Views;
using UI.Views.FactoryMenu;
using UnityEngine;
using Utilities.UI;

namespace Managers
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Canvas m_Canvas;
        
        [SerializeField] private List<PopUpMenu> m_PopUpPrefabs;
        [SerializeField] private List<ViewMenu> m_ViewPrefabs;

        [SerializeField] private Transform m_ViewContainer;
        [SerializeField] private Transform m_PopUpContainer;

        [SerializeField] private FactoryView m_FactoryView;
        [SerializeField] private SectorMapView m_SectorMapView;

        [SerializeField] private PageSwiper m_PageSwiper;

      

        public ViewController ViewController { get; private set; }
        public PopUpController PopUpController { get; private set; }

        public Action OnPlayButtonClickSound;
        public Action OnProblemSolveSound;
        public Action<AudioGroup, bool> OnSettingsButtonPressed;

        public void Initialize(UISoundsController uiSoundsController)
        {
            OnPlayButtonClickSound = uiSoundsController.OnPlayButtonClickSound;
            OnProblemSolveSound = uiSoundsController.OnProblemSolveSound;
            
            ViewController = new ViewController(this,m_ViewPrefabs,m_ViewContainer, m_FactoryView, m_SectorMapView,m_PageSwiper, m_Canvas);
            PopUpController = new PopUpController(this,m_PopUpPrefabs, m_PopUpContainer);
        }
    }
}