﻿using System;
using System.Linq;
using Core.Mechanics.CallSystem;
using Core.Mechanics.ProblemSystem;
using Core.Mechanics.SectorSystem;
using Services.InGameNotificationSystem;
using SFX;
using UnityEngine;

namespace Managers
{
    public class ProblemManager : MonoBehaviour, IProblemServiceStatistics
    {
        [SerializeField] private ProblemController m_ProblemController;
        [SerializeField] private CallController m_CallController;

        private IProblemSystemSectorController _sectorController;
        public double PlayerCustomerServiceRating { get; private set; }

        public void Setup(double playerCustomerServiceRating, IProblemSystemSectorController sectorController,
            NotificationController notificationController, UISoundsController uiSoundsController)
        {
            _sectorController = sectorController;
            PlayerCustomerServiceRating = playerCustomerServiceRating;

            m_CallController.Initialize(uiSoundsController.OnPlayButtonClickSound);
            m_CallController.OnCallAccepted += (problem) =>
            {
                m_ProblemController.AddProblem(problem);
                m_ProblemController.AddProblemIcon(problem);
            };
            m_CallController.OnCallDeclined += (problem) =>
            {
                PlayerCustomerServiceRating -= 0.2f;
                m_ProblemController.DeleteUnacceptedProblem(problem);
            };

            m_ProblemController.OnProblemGenerated += (problem) =>
            {
                Debug.Log(problem);
                notificationController.AddNotification($"Problem is Generated in Sector{problem.SectorID}",
                    NotificationNature.WarningNotification);
                if (_sectorController.PlayerSectorIDs.Contains(problem.SectorID))
                    m_CallController.EnqueueCall(problem);
                else
                {
                    //
                    //m_ProblemController.AddProblem(problem);
                }
            };

            m_ProblemController.OnProblemUnsuccessful += (problem) =>
            {
                if (_sectorController.PlayerSectorIDs.Contains(problem.SectorID))
                    PlayerCustomerServiceRating = Mathf.Clamp((float) PlayerCustomerServiceRating - 0.2f, 0f, 5f);
                else
                {
                }
            };
            m_ProblemController.OnProblemSuccessful += (problem) =>
            {
                if (_sectorController.PlayerSectorIDs.Contains(problem.SectorID))
                    PlayerCustomerServiceRating = Mathf.Clamp((float) PlayerCustomerServiceRating + 0.2f, 0f, 5f);
                else
                {
                }
            };
            m_ProblemController.OnProblemPass += _sectorController.AddProblemToSector;
            m_ProblemController.OnProblemEnded += _sectorController.RemoveProblem;
            m_ProblemController.OnGetAvailableSectorIDs += () => _sectorController.FreeSectorIDs.ToList();
            m_ProblemController.Setup();
        }
    }

    public interface IProblemServiceStatistics
    {
        double PlayerCustomerServiceRating { get; }
    }
}