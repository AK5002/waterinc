﻿using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;

namespace Services.DependencyInjection
{
    public class Injector : MonoBehaviour
    {
        [SerializeField] private List<MonoBehaviour> m_MonoObjects;

        public static Injector Instance { get; private set; }

        private Dictionary<string, IInjectable> AllInjectables;
        
        public void Initialize()
        {
            Instance = this;
            AllInjectables = new Dictionary<string, IInjectable>();
            
            foreach (var monoObject in m_MonoObjects)
            {
                if (monoObject is IInjectable injectable)
                {
                    AllInjectables.Add(injectable.GetType().Name, injectable);
                }
            }
        }

        public T GetInstanceOf<T>() where T: class,IInjectable
        {
            var retVal = AllInjectables[typeof(T).Name] as T;
            
            if(retVal == null)
                Debug.LogError("Could not find Requested Instance");

            return retVal;
        }
    }
}