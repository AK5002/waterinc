﻿using Services.InGameNotificationSystem.Notifications;
using UnityEngine;

namespace Services.InGameNotificationSystem
{
    public class NotificationFactory : MonoBehaviour
    {
        [SerializeField] private ErrorNotification m_ErrorNotification;
        [SerializeField] private WarningNotification m_WarningNotification;
        [SerializeField] private PositiveNotification m_PositiveNotification;

        public Notification Create(NotificationNature nature, Transform parent)
        {
            Notification notification = default;

            switch (nature)
            {
                case NotificationNature.ErrorNotification:
                    notification = Instantiate(m_ErrorNotification,parent);
                    break;
                case NotificationNature.WarningNotification:
                    notification = Instantiate(m_WarningNotification,parent);
                    break;
                case NotificationNature.PositiveNotification:
                    notification = Instantiate(m_PositiveNotification, parent);
                    break;
            }

            return notification;
        }
    }
}