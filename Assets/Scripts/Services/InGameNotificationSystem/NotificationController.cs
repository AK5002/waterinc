using System.Collections.Generic;
using DG.Tweening;
using Services.InGameNotificationSystem.Notifications;
using UnityEngine;

namespace Services.InGameNotificationSystem
{
    public class NotificationController:MonoBehaviour
    {
        [SerializeField] private NotificationFactory m_Factory;
        [SerializeField] private float m_Duration;
        [SerializeField] private int m_ActiveNotificationsLimit;
        
        private Queue<Notification> CurrentNotifications = new Queue<Notification>();

        public void AddNotification(string text, NotificationNature nature)
        {
            if (CurrentNotifications.Count >= m_ActiveNotificationsLimit) return;

            var notification = m_Factory.Create(nature, transform);
            MoveCurrentNotifications();
            notification.OnShow(text,m_Duration,OnNotificationDestroyedHandler);
            CurrentNotifications.Enqueue(notification);
        }
        
        private void MoveCurrentNotifications()
        {
            foreach (var notification in CurrentNotifications)
            {
                notification.RectTransform.DOAnchorPos(notification.RectTransform.anchoredPosition +new Vector2(0,-50),1f).SetEase(Ease.InSine);
            }
        }

        private void OnNotificationDestroyedHandler(Notification notification)
        {
            if(CurrentNotifications.Peek() != notification) return;
            CurrentNotifications.Dequeue();
        }
    }
}