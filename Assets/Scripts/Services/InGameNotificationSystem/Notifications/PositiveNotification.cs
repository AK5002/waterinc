﻿using System;
using UnityEngine;

namespace Services.InGameNotificationSystem.Notifications
{
    public class PositiveNotification : Notification
    {
        public override void OnShow(string text, float duration, Action<Notification> onNotificationDestroyed)
        {
            base.OnShow(text, duration, onNotificationDestroyed);
            m_Text.color = Color.green;
        }
    }
}