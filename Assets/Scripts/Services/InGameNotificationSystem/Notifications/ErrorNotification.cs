﻿using System;
using UnityEngine;

namespace Services.InGameNotificationSystem.Notifications
{
    public class ErrorNotification : Notification
    {
        public override void OnShow(string text, float duration, Action<Notification> onNotificationDestroyed)
        {
            base.OnShow(text, duration, onNotificationDestroyed);
            m_Text.text = "Error: " + text;
            m_Text.color = Color.red;
        }
    }
}