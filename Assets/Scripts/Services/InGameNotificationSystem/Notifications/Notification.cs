using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Services.InGameNotificationSystem.Notifications
{
    public abstract class Notification : MonoBehaviour
    {
        [SerializeField] protected RectTransform m_RectTransform;
        [SerializeField] protected CanvasGroup m_CanvasGroup;
        [SerializeField] protected TMP_Text m_Text;

        public RectTransform RectTransform => m_RectTransform;

        public virtual void OnShow(string text, float duration, Action<Notification> onNotificationDestroyed)
        {
            m_CanvasGroup.alpha = 0;
            Sequence notificationSequence = DOTween.Sequence();
            notificationSequence.Append(m_CanvasGroup.DOFade(1, duration/2).SetEase(Ease.InSine))
                .Append(m_CanvasGroup.DOFade(0, duration/2).SetEase(Ease.InSine).SetDelay(2f)).OnComplete(() =>
                {
                    Destroy(gameObject);
                    onNotificationDestroyed(this);
                });
        }
    }
}