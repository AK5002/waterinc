﻿namespace Services.InGameNotificationSystem
{
    public enum NotificationNature
    {
        ErrorNotification,
        WarningNotification,
        PositiveNotification
    }
}