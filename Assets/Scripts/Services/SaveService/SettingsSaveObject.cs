﻿using System;

namespace Services.SaveService
{
    [Serializable]
    public class SettingsSaveObject : ISavable
    {
        public bool IsSoundOn;
        public bool IsMusicOn;
        public bool IsEffectsOn;
    }
}