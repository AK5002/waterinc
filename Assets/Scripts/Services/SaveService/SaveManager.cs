﻿using System;
using System.Collections.Generic;
using System.IO;
using Core.Game.ProductionLine;
using Core.Game.ProductionLine.Config;
using Core.Mechanics.SectorSystem;
using Newtonsoft.Json;
using UnityEngine;

namespace Services.SaveService
{
    public class SaveManager
    {
        public SaveObject CurrentSaveObject { get; private set; }
        public SettingsSaveObject CurrentSettings { get; private set; }

        private string saveFilePath;
        private string settingsFilePath;

        private SaveObject defaultSaveObject = new SaveObject()
        {
            Money = 0f,
            CustomerServiceRating = 0f,
            SaveDatas = new List<ConveyorSaveData>(new ConveyorSaveData[1]
            {
                new ConveyorSaveData()
                {
                    ID = 0,
                    PumpID = 0,
                    PlantID = 0,
                    AvailablePumpIDs = new List<int>(new int[1] {0}),
                    AvailablePlantIDs = new List<int>(new int[1] {0}),
                }
            }),
        };
        
        private SettingsSaveObject defaultSettings = new SettingsSaveObject()
        {
            IsSoundOn = true,
            IsMusicOn = true,
            IsEffectsOn = true,
        };

        public void Initialize()
        {
            saveFilePath = Path.Combine(Application.persistentDataPath, "SaveGame.json");
            settingsFilePath = Path.Combine(Application.persistentDataPath, "Settings.json");

            if (!File.Exists(saveFilePath))
            {
                Debug.Log("Creating new SaveFile");
                CreateSaveFile();
            }
            
            if (!File.Exists(settingsFilePath))
            {
                CreateSettingsFile();
            }

            CurrentSaveObject = Deserialize<SaveObject>(new StreamReader(saveFilePath));
            CurrentSettings = Deserialize<SettingsSaveObject>(new StreamReader(settingsFilePath));
        }

        public void SaveGame(double money, double customerServiceRating, List<ConveyorSaveData> saveDatas,
            List<SectorData> sectorDatas)
        {
            var saveObject = new SaveObject()
            {
                Money = Math.Floor(money),
                CustomerServiceRating = customerServiceRating,
                SaveDatas = saveDatas,
                SectorDatas = sectorDatas,
            };
            Serialize(saveObject, new StreamWriter(saveFilePath));
        }

        public void SaveSettings(bool isSoundOn, bool isMusicOn, bool isEffectsOn)
        {
            var settings = new SettingsSaveObject()
            {
                IsSoundOn = isSoundOn,
                IsMusicOn = isMusicOn,
                IsEffectsOn = isEffectsOn
            };
            Serialize(settings,new StreamWriter(settingsFilePath));
        }

        private void CreateSaveFile()
        {
            CurrentSaveObject = defaultSaveObject;
            Serialize(defaultSaveObject, new StreamWriter(saveFilePath));
        }
        
        private void CreateSettingsFile()
        {
            CurrentSettings = defaultSettings;
            Serialize(defaultSettings, new StreamWriter(settingsFilePath));
        }

        private void Serialize<T>(T saveObject, StreamWriter file) where T : ISavable
        {
            WriteContent(JsonConvert.SerializeObject(saveObject), file);
        }

        private T Deserialize<T>(StreamReader file) where T : ISavable
        {
            T retval = JsonConvert.DeserializeObject<T>(ReadContent(file));
            return retval;
        }

        private string ReadContent(StreamReader file)
        {
            string result = default;

            using (file)
            {
                result = file.ReadToEnd();
            }

            return result;
        }

        private void WriteContent(string text, StreamWriter file)
        {
            using (file)
            {
                file.Write(text);
            }
        }
    }
}