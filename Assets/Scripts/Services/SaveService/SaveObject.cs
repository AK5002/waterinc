﻿using System;
using System.Collections.Generic;
using Core.Game.ProductionLine;
using Core.Game.ProductionLine.Component;
using Core.Game.ProductionLine.Config;
using Core.Mechanics.SectorSystem;

namespace Services.SaveService
{
    [Serializable]
    public class SaveObject : ISavable
    {
        public double Money;
        public double CustomerServiceRating;
        public List<ConveyorSaveData> SaveDatas;
        public List<SectorData> SectorDatas;
    }
}