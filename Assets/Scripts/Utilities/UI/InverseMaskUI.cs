﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Utilities.UI
{
    public class InverseMaskUI : Image
    {
        public override Material materialForRendering
        {
            get
            {
                var newMaterial = new Material(base.materialForRendering);
                newMaterial.SetInt("_StencilComp", (int) CompareFunction.NotEqual);
                return newMaterial;
            }
        }
    }
}