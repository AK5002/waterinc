﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace Utilities.UI
{
    public class PagepointController : MonoBehaviour
    {
        [SerializeField] private List<RectTransform> m_PagePointHolders;
        [SerializeField] private RectTransform m_PagePoint;

        private int pageIndex;

        public void Setup(int pageIndex)
        {
            this.pageIndex = pageIndex;
            m_PagePointHolders = m_PagePointHolders.OrderBy(holder => holder.position.x).ToList();
            m_PagePoint.position = m_PagePointHolders[pageIndex].position;
        }

        public void MoveTo(int direction)
        {
            pageIndex += direction;
            var toPosition = m_PagePointHolders[pageIndex].position;
            m_PagePoint.DOMove(toPosition, 0.5f).SetEase(Ease.InSine);
        }
        
    }
}