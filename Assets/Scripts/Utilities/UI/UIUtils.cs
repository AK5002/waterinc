﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utilities.UI
{
    public static class UIUtils
    {
        public static readonly SortedDictionary<int, string> DECIMAL_NOTATIONS = new SortedDictionary<int, string>()
        {
            {0, ""},
            {3, "K"},
            {6, "M"},
            {9, "B"},
            {12, "T"},
            {15, "Q"}
        };

        public static string NumberToDecimalNotation(string num, int floatPrecision)
        {
            var result = "";
            var i = 0;
            if (num[0] == '-')
            {
                result += num[0];
                i++;
            }

            string notationLetter = "";
            int decimalCount = ((num.Length - 1) / 3) * 3;
            if(decimalCount != 0)
                notationLetter += DECIMAL_NOTATIONS[decimalCount];
            for (; i < Math.Min(num.Length - decimalCount + floatPrecision, num.Length); i++)
            {
                if (i == num.Length - decimalCount)
                    result += ".";

                result += num[i];
            }

            return result + notationLetter;
        }
    }
}