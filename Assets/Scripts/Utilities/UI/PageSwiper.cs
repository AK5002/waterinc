﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Utilities.UI
{
    public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
    {
    	[SerializeField] private Canvas m_Canvas;
        [SerializeField] private List<RectTransform> m_Pages;
        [SerializeField] private float m_PercentageThreshold;
        [SerializeField] private float m_Easing;
        [SerializeField] private PagepointController pagepointController;

        private Vector2 panelLocation;
        private int PageCount => m_Pages.Count;
        
        public event Action<SwipeDirection> OnSwipeComplete;
        public int PageIndex { get; private set; }
        
        public void Awake()
        {
            PageIndex = 0;
            pagepointController.Setup(PageIndex); 
            panelLocation = transform.position;

            var offset = Vector2.zero;

            foreach (var page in m_Pages)
            {
                page.anchoredPosition += offset;
                offset += new Vector2(Screen.width / m_Canvas.scaleFactor,0);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            var difference = eventData.pressPosition.x - eventData.position.x;
            
            if (difference < 0 && PageIndex == 0) return;
            if (difference > 0 && PageIndex == PageCount - 1) return;
            
            transform.position = panelLocation - new Vector2(difference, 0);

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            var percentage = (eventData.pressPosition.x - eventData.position.x) / Screen.width;
            var direction = percentage > 0 ? SwipeDirection.Right : SwipeDirection.Left; 
            
            if (Mathf.Abs(percentage) >= m_PercentageThreshold && ((PageIndex + direction) >= 0 && (PageIndex+(int)direction) < PageCount))
            {
                var newLocation = panelLocation;
                
                newLocation += new Vector2(-(int)direction * Screen.width,0);
                
                var transformMove = new TransformMoveInstruction(this,transform,transform.position,newLocation,m_Easing);
                pagepointController.MoveTo((int)direction);
                transformMove.OnDone += () =>
                {
                    panelLocation = newLocation;
                    PageIndex += (int)direction;
                    
                    OnSwipeComplete?.Invoke(direction);
                };
                transformMove.Execute();
                
            }
            else
            {
                var transformMove = new TransformMoveInstruction(this,transform,transform.position,panelLocation,m_Easing);
                transformMove.Execute();
                pagepointController.MoveTo(0);
            }
        }

        public void SwipeToNextPage()
        {
            var direction = SwipeDirection.Right;
            var newLocation = panelLocation;
                
            newLocation += new Vector2(-(int)direction * Screen.width,0);
                
            var transformMove = new TransformMoveInstruction(this,transform,transform.position,newLocation,m_Easing);
            transformMove.OnDone += () =>
            {
                panelLocation = newLocation;
                PageIndex += (int)direction;
                OnSwipeComplete?.Invoke(direction);
            };
            transformMove.Execute();
        }

        public void SwipeToPreviousPage()
        {
            var direction = SwipeDirection.Left;
            var newLocation = panelLocation;
                
            newLocation += new Vector2(-(int)direction * Screen.width,0);
                
            var transformMove = new TransformMoveInstruction(this,transform,transform.position,newLocation,m_Easing);
            transformMove.OnDone += () =>
            {
                panelLocation = newLocation;
                PageIndex += (int)direction;
                OnSwipeComplete?.Invoke(direction);
            };
            transformMove.Execute();
        }
    }

    public enum SwipeDirection
    {
        Left = -1,
        Right = 1,
    }
}