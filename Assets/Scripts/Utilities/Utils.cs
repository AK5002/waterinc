﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using Utilities.CustomYieldInstruction;

namespace Utilities
{
    public class TransformMoveInstruction : Instruction
    {
        private readonly Transform _transform;
        private readonly Vector3 _startPos;
        private readonly Vector3 _endPos;
        private readonly float _duration;
        private float t;

        public event Action OnDone;
        
        public TransformMoveInstruction(MonoBehaviour parent, Transform transform,Vector3 startPos,Vector3 endPos, float duration) : base(parent)
        {
            _transform = transform;
            _startPos = startPos;
            _endPos = endPos;
            _duration = duration;
        }

        protected override bool Update()
        {
            t += Time.deltaTime / _duration;
            _transform.position = Vector3.Lerp(_startPos, _endPos, Mathf.SmoothStep(0f, 1f, t));
            return t <= 1;
        }

        protected override void OnInstructionDone()
        {
            OnDone?.Invoke();
        }
    }

    public static class FitInSafeAreaExtension
    {
        public static void FitInSafeArea(this RectTransform rectTransform)
        {
            Rect safeArea = UnityEngine.Screen.safeArea;
    
            Vector2 anchorMin = safeArea.position;
            Vector2 anchorMax = safeArea.position + safeArea.size;
    
            anchorMin.x /= UnityEngine.Screen.width;
            anchorMin.y /= UnityEngine.Screen.height;
            anchorMax.x /= UnityEngine.Screen.width;
            anchorMax.y /= UnityEngine.Screen.height;
    
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
        }
    }
}